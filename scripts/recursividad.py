# -*- coding: utf-8 -*-
"""
Created on Fri Dec  4 08:48:40 2020

@author: Lola Serrano
"""

'''Algunos ejemplos de cómo funciona la recursividad
y sus equivalentes en iteratividad'''

l = [3,1,2,4,6,1,2]
lista_vacia = []
suma = 0
suma2 = 0
#Dos formas de escribir lo mismo
for i in l:
    suma += i
for i in range(len(l)):
    suma2 += l[i]
    
#Lo mismo de arriba pero en una función
def suma(l):
    resultado = 0
    for i in l:
        resultado += i
    return resultado

print(suma(lista_vacia))

#Una función suma pero usando recursividad
def sumaR(l):
    if len(l) == 0:
        return 0
    else:
        return l[0] + sumaR(l[1:])
        
#La empezamos a sumar a partir del último elemento
def sumaR2(l):
    if len(l) == 0:
        return 0
    else:
        return l[-1] + sumaR(l[:-1])
        
def fibonacci(n):
    if n == 1:
        return 0
    elif n == 2:
        return 1
    else:
        return fibonacci(n-1)+fibonacci(n-2)
print(fibonacci(8))

def Factorial_recursivo(n):
    if n == 0:
        return 1
    else:
        return n*Factorial_recursivo(n-1)

def Factorial_iterativo(n):
    fac = 1
    for i in range(n+1):
        fac = fac*1
    return fac

print(Factorial_recursivo(5))