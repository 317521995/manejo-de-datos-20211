# -*- coding: utf-8 -*-
"""
Created on Mon Nov 30 08:08:02 2020

@author: LAP-HP
"""
#Vamos a aprender algoritmos de ordenamiento


lol = [7, 13, 15, 9, 2]
l2 = [4,3,5,1,2,3,5,0]
l = [9,8,7,6,5,4,3,2,1,0,50]
n = len(lol)
#%%
'''Esta es una manera de comparar los datos donde checamos en dos posiciones seguidas
si uno es más grande que otro, si sí se intercambian de lugar. Si no, se quedan igual.
Todo esto está bien pero es muy manual para una lista en particular. Se puede generalizar 
para cualquier lista.'''
# if l[0]>l[1]:
#     aux = l[0]
#     l[0] = l[1]
#     l[1] = aux

# if l[1] > l[2]:
#     aux = l[1]
#     l[1] = l[2]
#     l[2] = aux

# if l[2] > l[3]:
#     aux = l[2]
#     l[2] = l[3]
#     l[3] = aux

# i = 3
# if l[i] > l[i+1]:
#     aux = l[i]
#     l[i] = l[i+1]
#     l[i+1] = aux
'''Aquí ya lo estamos haciendo en general para la lista lol, usando iteratividad'''
for i in range(n-1):
    if lol[i] > lol[i+1]:
        lol[i], lol[i+1] = lol[i+1], lol[i]
#%%
'''Lo generalizamos aún más con una función. Este algoritmo se llama 
bubble sort, o burbuja. Esta función sirve para una lista cualquiera'''
        
l = [7, 13, 15, 9, 2]
l2 = [9,8,7,6,5,4,3,2,1,0,50]
def burbuja(l:list):
    n = len(l)
    for j in range(n-1):
        for i in range(n-1):
            if l[i] > l[i+1]:
                l[i], l[i+1] = l[i+1], l[i]
                # aux = l[i]
                # l[i] = l[i+1]
                # l[i+1] = aux
        n -= 1
    return l
print(burbuja(l))
print(burbuja(l2))

def burbuja_bidireccional(l:list):
    n = len(l)
    for j in range(n-1):
        for i in range(n-1):
            if l[i] < l[i+1]:
                l[i], l[i+1] = l[i+1], l[i]

