#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May 30 13:22:26 2020

@author(es): 
Kevin Hernández Astudillo
Iván Organista Dionicio
Lola Serrano Carrasco
"""
from clases.Geometria_Analitica.Conicas import EcuacionGeneral
'''
En Spyder
Herramientas -> Administrador del PYTHONPATH -> 
Añadir ruta -> navegar hasta el repositorio dentro de Documentos


Desde anaconda Prompt:
Windows
    set PYTHONPATH=c:\\Users\<El nombre del usuario>\Docuents\manejo-de-datos-20211
Linux
    export PYTHONPATH=/home/<nombre de usuario>/Documentos/manejo-de-datos-20211
'''

'''IMPORTANTE: TAL VEZ SE DEBA DE INSTALAR ALGUNA BIBLIOTECA.
Recordar usar "pip install -nombre de la biblioteca-" en el prompt de Anaconda 
(O donde sea que esté instalado Python) para PODER HACER USO DE LOS SIGUIENTES MÉTODOS'''
import tkinter as tk
from tkinter import *
import math
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from tabulate import tabulate

root = Tk()
root.title("Interfaz_AplicacionPOO")
#Comentar la línea de abajo a menos que tengas algún archivo '.ico' para ponerlo de ícono
#root.iconbitmap('C:/Users/RUTA PARA ACCEDER AL DOCUMENTO .ico/manejo-de-datos-20211/clases/Geometria_Analitica/leaf_plant.ico')
root.geometry('1400x650')

def Ingresar():
    global A
    global B
    global C
    global D
    global E
    global F
    try:
        float(Entrada_A.get())
        float(Entrada_B.get())
        float(Entrada_C.get())
        float(Entrada_D.get())
        float(Entrada_E.get())
        float(Entrada_F.get())
        respuesta.config(text='Todos tus valores son flotantes :D') 
        A= float(Entrada_A.get())
        B= float(Entrada_B.get())
        C= float(Entrada_C.get())
        D= float(Entrada_D.get())
        E= float(Entrada_E.get())
        F= float(Entrada_F.get())
        Ecuacion_str = "Tu Ecuación General es de la forma:\n " +str(A)+"x^2 + "+str(B)+"xy + "+str(C)+"y^2 + "+str(D)+"x + "+str(E)+"y + "+str(F)+"= 0"
        Letrero_Ecuacion_str = Label(root, text=Ecuacion_str)
        Letrero_Ecuacion_str.grid(row=2, column=1)
        #Ecuacion = EcuacionGeneral(A,B,C,D,E,F)
        #Label_Ecuacion = Label(root, text=Ecuacion)
        #Label_Ecuacion.grid(row=3, column =3)
    except ValueError:
        respuesta.config(text='¡Todos tus valores tienen que ser enteros o flotantes!')

def Conica():
    try:
        Ecuacion = EcuacionGeneral(A,B,C,D,E,F)
        Letrero_Conica = Label(root, text=" Con los datos que ingresaste obtuviste que:\n" + str(Ecuacion.QueConicaEs()))
        Letrero_Conica.grid(row=10, column=3)
        btn = Button(root, text="¡Grafica tu ecuación!", command=Graficar, pady=40, padx=30)
        btn.grid(row=9,column=4)
    except NameError:
        Letrerror = Label(root, text="Primero tienes que registrar tu Ecuación General correctamente")
        Letrerror.grid(row=4, column=3)

def Graficar():
    Ecuacion = EcuacionGeneral(A,B,C,D,E,F)
    print(Ecuacion.Graficarla())
    Letrerog = Label(root, text="Si por alguna razón no se puede graficar \n(Ver la terminal para ver qué regresa),\n te invitamos a probar algún ejemplo\n(O ingresar una Ecuación General más popular)")
    Letrerog.grid(row=10, column=4)

def Ejemplo1(): #Elipse
    A = 2
    B = 0
    C = 3
    D = 4
    E = 5
    F = 0
    h1=EcuacionGeneral(A,B,C,D,E,F) #Elipse
    Letrero_h1 = Label(root, text="El ejemplo 1 contiene la siguiente Ecuación:\n" + str(h1) +"\n tal que \n" + str(h1.QueConicaEs()))
    Letrero_h1.grid(row=2,column=3)
    print(h1.Graficarla())
    tabla = [['Categoria', 'Centro', 'Focos', 'Vértices', 'Canónica'], 
            [str(h1.Elipse()[0]),str(h1.Elipse()[1]),str(h1.Elipse()[2]),str(h1.Elipse()[3]),str(h1.Elipse()[4])]]
    #Para configurar los datos de la 2a ventana
    top = Toplevel() # 'Toplevel()' Crea otra ventana "secundaria"
    top.title("La 2a ventana para la Tabla")
    h1.Elipse()
    Letrero_Tabla = Label(top, text=tabulate(tabla, headers = 'firstrow', tablefmt = 'fancy_grid'))
    Letrero_Tabla.pack()
    boton_2 = Button(top, text="Cerrar ventana", command=top.destroy)
    boton_2.pack()

def Ejemplo2(): #Parábola
    A = 6
    B = 0
    C = 0
    D = 5
    E = -4
    F = 8
    p2=EcuacionGeneral(A,B,C,D,E,F)
    Letrero_p2 = Label(root, text="El ejemplo 2 contiene la siguiente Ecuación:\n" + str(p2) +"\n tal que \n" + str(p2.QueConicaEs()))
    Letrero_p2.grid(row=2,column=4)
    print(p2.Graficarla())
    tabla = [['Categoria', 'Centro', 'Focos', 'Vértices', 'Canónica'], 
            [str(p2.Parabola()[0]),str(p2.Parabola()[1]),str(p2.Parabola()[2]),str(p2.Parabola()[3])] ]
    #return tabulate(tabla, headers = 'firstrow', tablefmt = 'fancy_grid') 
    #Para configurar los datos de la 2a ventana
    top = Toplevel() # 'Toplevel()' Crea otra ventana "secundaria"
    top.title("La 2a ventana para la Tabla")
    p2.Parabola()
    Letrero_Tabla = Label(top, text=tabulate(tabla, headers = 'firstrow', tablefmt = 'fancy_grid'))
    Letrero_Tabla.pack()
    boton_2 = Button(top, text="Cerrar ventana", command=top.destroy)
    boton_2.pack()

def Ejemplo3(): #Hipérbola
    A = 90
    B = 0
    C = -10
    D = -900
    E = 160
    F = 1529
    h3=EcuacionGeneral(A,B,C,D,E,F)
    Letrero_h3 = Label(root, text="El ejemplo 3 contiene la siguiente Ecuación:\n" + str(h3) +"\n tal que \n" + str(h3.QueConicaEs()))
    Letrero_h3.grid(row=2,column=5)
    print(h3.Graficarla())
    tabla = [['Categoria', 'Centro', 'Focos', 'Vértices', 'Canónica'], 
            [str(h3.Hiperbola()[0]),str(h3.Hiperbola()[1]),str(h3.Hiperbola()[2]),str(h3.Hiperbola()[3]),str(h3.Hiperbola()[4])] ]
    #return tabulate(tabla, headers = 'firstrow', tablefmt = 'fancy_grid')
    #Para configurar los datos de la 2a ventana
    top = Toplevel() # 'Toplevel()' Crea otra ventana "secundaria"
    top.title("La 2a ventana para la Tabla")
    h3.Hiperbola()
    Letrero_Tabla = Label(top, text=tabulate(tabla, headers = 'firstrow', tablefmt = 'fancy_grid'))
    Letrero_Tabla.pack()
    boton_2 = Button(top, text="Cerrar ventana", command=top.destroy)
    boton_2.pack()



#Para el 'frame de bienvenida'
frame = LabelFrame(root, text="¡Bienvenid@ a nuestra interfaz!", fg="blue")
frame.grid(row=0, column=3, columnspan=5)
#Para añadir texto y botones al frame=marco
bienvenida = Label(frame, text="Ingresa los coeficientes de tu Ecuación General\n Ax^2+Bxy+Cy^2+Dx+Ey+F=0\n Si no ingresas los 6 elementos (Coeficientes) como enteros o flotantes, no podrás continuar con el programa,\n ¡Ingresa flotantes o enteros!  E.g.: '(1,2.2,3.3,4.4,5.5,6)'")
bienvenida.grid(row=1, column=1)
b = Button(frame, text="¡Pruébame! (Ejemplo 1)", command=Ejemplo1)
b2 = Button(frame, text="¡A mi también! (Ejemplo 2)", command=Ejemplo2)
b3 = Button(frame, text="A mi no me cliquees ¬¬ (Ejemplo 3)", command=Ejemplo3)
b.grid(row=2, column=0) #DENTRO DE LOS FRAMES ES POSIBLE PONER "grid" DENTRO DE UN ".pack()"
b2.grid(row=2, column=1)
b3.grid(row=2, column=2)


respuesta = Label(root, text='')
respuesta.grid(row=9, column=3)
#Las entradas para todos los valores de los coeficientes
Entrada_A = Entry(root)
Entrada_A.grid(row=3,column=1)
Entrada_B = Entry(root)
Entrada_B.grid(row=4,column=1)
Entrada_C = Entry(root)
Entrada_C.grid(row=5,column=1)
Entrada_D = Entry(root)
Entrada_D.grid(row=6,column=1)
Entrada_E = Entry(root)
Entrada_E.grid(row=7,column=1)
Entrada_F = Entry(root)
Entrada_F.grid(row=8,column=1)
#Texto para indicar coeficientes:
Letrero_A = Label(root, text="Coeficiente 'A':")
Letrero_A.grid(row=3, column=0)
Letrero_B = Label(root, text="Coeficiente 'B':")
Letrero_B.grid(row=4, column=0)
Letrero_C = Label(root, text="Coeficiente 'C':")
Letrero_C.grid(row=5, column=0)
Letrero_D = Label(root, text="Coeficiente 'D':")
Letrero_D.grid(row=6, column=0)
Letrero_E = Label(root, text="Coeficiente 'E':")
Letrero_E.grid(row=7, column=0)
Letrero_F = Label(root, text="Coeficiente 'F':")
Letrero_F.grid(row=8, column=0)
#Letrero_Aviso
Letrero_Aviso = Label(root, text="Obviamente este programa no es perfecto, de hecho, \n probablemente las gráficas no salgan muy acomodadas, \n pero bueno, ¡Diviértete!")
Letrero_Aviso.grid(row=14, column =5)

#Botones
Boton_Entrada = Button(root, text="Registra tu Ecuación General", command=Ingresar, pady=30, padx=15)
Boton_Entrada.grid(row=9, column=1)
Boton_QueConicaEs = Button(root, text="¿Qué cónica forman tus valores?", command = Conica, pady=30, padx=15)
Boton_QueConicaEs.grid(row = 10, column=1)
#Botón "Salir del programa"
Boton_salir = Button(root, text="Salir del programa", command=root.quit, pady=35, padx=10)
Boton_salir.grid(row=15, column=5)
root.mainloop()

#Ecuacion = EcuacionGeneral(A,B,C,D,E,F)
#Ecuacion.Graficarla()

"""
#%%
#Algunos Ejemplos:
#Para e1
print("-----Para e1----------------"*3) 
e1 = EcuacionGeneral(2,0,4,12,6,5) #Elipse
print(e1.QueConicaEs())
print(e1.Elipse())
Conica_e = e1.QueConicaEs()
print(Conica_e)
print(e1.EliminarTerminoCruzado())
print(e1.Tabla())
print(e1.Graficarla())
#Para e2
print("-----Para e2----------------"*3)
e2=EcuacionGeneral(1,0,1,13,-21,-4) #Circunferencia
e2.Circunferencia()
print(e2)
print(e2.QueConicaEs())
print(e2.EliminarTerminoCruzado())
print(e2.Graficarla())
#Para la parábola 'p1'
p1 = EcuacionGeneral(6,0,0,5,-4, 8) #Parábola
print(p1.Parabola())
print(p1.Tabla())
print(p1*3)
print(p1/2)
print(p1.EliminarTerminoCruzado())
print(p1.Graficarla())

#%%
print("-M-Á-S--E-J-E-M-P-L-O-S--"*4)
#solo son prubas para ver que tdo jala
#Para h4
print("-----Para h4---------------"*3)
h4 = EcuacionGeneral(5,8,3,2,2) #Hipérbola**
print(h4)
Conica_4 = h4.QueConicaEs()
print(Conica_4)
print(h4.EliminarTerminoCruzado()) #DEGENERA ESTA HIPÉRBOLA.
print(h4.Graficarla())
#Para h5
print("-----Para h5----------------"*3)
h5 = EcuacionGeneral(5,8,3,2,2,-15) #Hipérbola**
print(h5)
Conica_5 = h5.QueConicaEs()
print(Conica_5)
print(h5.EliminarTerminoCruzado()) #DEGENERA ESTA HIPÉRBOLA.
print(h5.Graficarla())

#Para el resto de variables
print("-----Para el resto (c1)---------------"*2)
c1=EcuacionGeneral(2,2,3,4,5) #Elipse
print(c1)
Conica_1 = c1.QueConicaEs()
print(Conica_1)
print(c1.EliminarTerminoCruzado())
print(c1.Graficarla())

#Para c2
print("-----Para c2-----------------"*3)
c2=EcuacionGeneral(1,2,1,1,4,-1) #No está bien definida.
print(c2)
Conica_2 = c2.QueConicaEs()
print(Conica_2)
print(c2.EliminarTerminoCruzado())
print(c2.Graficarla())

#Para c3
print("-----Para c3-----------------"*3)
c3=EcuacionGeneral(1,3,1) #Hipérbola**
print(c3)
Conica_3 = c3.QueConicaEs()
print(Conica_3)
print(c3.EliminarTerminoCruzado())
print(c3.Graficarla())

#Para c4
print("-----Para c4-----------------"*3)
c4=EcuacionGeneral(0,0,0,1,2,1) #NO ES CÓNICA (Tiene A=B=C=0)
print(c4)
Conica_4 = c4.QueConicaEs()
print(Conica_4)
print(c4.EliminarTerminoCruzado())
print(c4.Graficarla())

#Para c5
print("-----Para c5-----------------"*3)
c5=EcuacionGeneral(4,0,4,-4,-8,-11) #Circunferencia
print(c5)
Conica_5 = c5.QueConicaEs()
print(Conica_5)
print(c5.EliminarTerminoCruzado())
print(c5.Graficarla())

#Para c6
print("-----Para c6-----------------"*3)
c6=EcuacionGeneral(1,0,1,2,2,1) #Circunferencia
c6.Circunferencia()
print(c6)
Conica_6 = c6.QueConicaEs()
print(Conica_6)
print(c6.EliminarTerminoCruzado())
print(c6.Graficarla())

#Para c7
print("-----Para c7-----------------"*3)
c7=EcuacionGeneral(1,0,1,2,-2,0) #Circunferencia
print(c7)
Conica_7 = c7.QueConicaEs()
print(Conica_7)
print(c7.EliminarTerminoCruzado())
print(c7.Graficarla())
#%%
#Para c8
print("-----Para c8-----------------"*3)
c8=EcuacionGeneral(5,0,5,1,2,0) #Circunferencia
print(c8)
Conica_8 = c8.QueConicaEs()
print(Conica_8)
print(c8.Circunferencia())
print(c8.EliminarTerminoCruzado())
print(c8.Graficarla())

#Para c9
print("-----Para c9-----------------"*3)
c9=EcuacionGeneral(1,2,3,4,5,6) #Elipse rotada = Hipérbola o_O
print(c9)
Conica_9 = c9.QueConicaEs()
print(Conica_9)
print(c9.EliminarTerminoCruzado()) #DEGENERA ESTA CÓNICA.
print(c9.Graficarla())

#Para c10
print("-----Para c10-----------------"*3)
circ=EcuacionGeneral(2,0,2,8,6,5) #Circunferencia
print(circ.Circunferencia())
Conica_c = circ.QueConicaEs()
print(Conica_c)
print(circ.EliminarTerminoCruzado())
print(circ.Tabla())
print(circ.Graficarla())
"""
#Fin