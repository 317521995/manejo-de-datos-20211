#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 11 17:15:36 2020

@author: luis
"""
import matplotlib.pyplot as plt #Importamos la biblioteca matplotlib pyplot.
import numpy as np #Le cambiamos el nombre el módulo importado.

#%% ¿Cómo crear una gráfica en Python?

#Importamos los módulos necesarios.
import math

#Generamos los datos para el gráfico.
x= np.array(range(20)) *0.1 #Creamos un arreglo con 20 puntos separados por 0.1. (Partición uniforme de 0 a .9)
y= np.zeros(len(x)) #Creamos un arreglo del mismo tamaño con ceros.
for i in range(len(x)):
    y[i] = math.sin(x[i])

#Creamos el gráfico.
plt.plot(x,y)


#%% Ejemplo de internet
# https://matplotlib.org/3.3.2/gallery/lines_bars_and_markers/simple_plot.html
# Data for plotting
t = np.arange(0.0, 2.0, 0.01) #Arreglo del intervalo de 0 a dos dividiento en partes de 0.01.
s = 1 + np.sin(2 * np.pi * t) #La función +seno(2pi*x)aplicado a cada uno de los intervalos

fig, ax = plt.subplots() #Function that returns a tuple containing a figure and axes object(s). Esta instrucción genera los ejes.
ax.plot(t, s) #Creamos el gráfico

ax.set(xlabel='time (s)', ylabel='voltage (mV)',
        title='About as simple as it gets, folks') #Definimos el título de la gráfica, así como los nombres de sus ejes.
ax.grid()

#fig.savefig("test.png")
plt.show()

#%% Ejemplo del profesor

#Preparación para escribir mi código en términos generales
def f(x): 
    return np.exp(x)-1

def g(x):
    return 0.5*(x+1/x)

def h(x):
    return np.sqrt(1-x**2)

#Definimos la animación
def animaXY(x, y, xa, xb, ya, yb, fig1,xl="",yl="",tit=""):
    """
    https://www.reddit.com/r/IPython/comments/8o5vft/animate_points_in_two_numpy_arrays_with_matplotlib/
    """
    import numpy as np
    
    import matplotlib.animation as animation
    from IPython.display import HTML
    
    def update_line(num, data, line):
        line.set_data(data[..., :num])
        c=['b','r','g','y']
        line.set_color(c[num%len(c)])        
        return line,
  
    d =np.array([x,y])  
    l, = plt.plot([], [], 'rp-')
    plt.xlim(xa, xb)
    plt.ylim(ya, yb)
    plt.xlabel(xl)
    plt.title(tit)
    line_ani = animation.FuncAnimation(fig1, update_line, len(x), fargs=(d, l),
                                       interval=200, blit=True)
    #line_ani.save('animación.gif', writer='imagemagick', fps=30)
    """
    Writer = animation.writers['ffmpeg']
    writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=1800)
    line_ani.save('im.mp4', writer=writer)
    """
    #HTML(line_ani.to_html5_video())  

def araña(a = 0,b = 1, n=50, funcion = h, m = 20 ,x0 = 0.2,xl="", yl="", tit=""):
    """def araña(intervalo a graficar punto de incio abierto a, punto de fin b cerrado, n = número de puntos a graficar,
            función a utilizar, m = número de escalones (elementos en lista),
            título eje x, título eje y, título gráfica"""
    #https://policonomics.com/es/modelo-telarana/ Una aplicación de la gráfica araña
    #n = 50
    #[a, b] = [0.0, 1.0]
    x = np.arange(a, b, (b-a)/n) #Crea un arreglo que inicia en a, termina en b y genera una partición uniforme de 1/n (se genera en forma de lista). Ya tengo las x.
    y = funcion(x) #Estas serán las y a graficar
    
    fig, ax = plt.subplots() #Estas son las instrucciones para hacer la gráfica
    ax.plot(x, y) #Nos da la curva
    ax.plot(x, x) #Nos da la identidad
    
    #Ahora graficamos los escalones
    X = [x0]
    Y = [funcion(x0)] #De forma "manual" asignamos los valores de los primeros puntos.
    #Ahora definimos el valor de x y y, los metemos en las listas.
    for i in range(m):
        # X.append(Y[-1])
        # Y.append(Y[-1])    
        # X.append(X[-1])
        # Y.append(funcion(X[-1]))
        X = X + [Y[-1], Y[-1]] #A la lista le añadimos las coordenadas x de los siguientes dos puntos.
        Y = Y + [Y[-1], funcion(X[-1])]  #A la lista y, le añadimos las coordenadas y de los siguientes dos puntos.
    
    
    # Si solo se quiere la gráfica estática 
    # descomentar las líneas 92 y 99 y comentar la 98
    #ax.plot(X, Y, '-o', color='black') 

    ax.set(xlabel=xl, ylabel=yl,
           title=tit)
    ax.grid()
    #animaXY(np.array(X),np.array(Y),a,b,min(Y),max(Y),fig,xl,yl,tit)
    animaXY(np.array(X),np.array(Y),a,b,min(Y),max(funcion(b),max(Y)),fig,xl,yl,tit)
    #plt.show()

#Llamado de funciones    
araña(0,1, 50, h, 20 ,0.2,'x','sqrt(1 - x²)',"Gráfica de sqrt(1 -x²)")
araña(-4.0, 1, 30, f, 20, -3.175,'x','e^(x)-1','Gráfica de e^(x)-1')
araña(0.1, 2.5, 50, g, 30, .3,'x', '0.5*(x+1/x)','Gráfica de 0.5*(x+1/x)')

