#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May 30 13:22:26 2020

@author: luis
"""
'''
En Spyder
Herramientas -> Administrador del PYTHONPATH -> 
Añadir ruta -> navegar hasta el repositorio dentro de Documentos


Desde anaconda Prompt:
Windows
    set PYTHONPATH=c:\\Users\<El nombre del usuario>\Docuents\manejo-de-datos-20211
Linux
    export PYTHONPATH=/home/<nombre de usuario>/Documentos/manejo-de-datos-20211
'''

from clases.ido.ProblemaDelTransporte import ciudad, planta, problema_transporte

Ciudades = [ciudad("Cali",70), ciudad("Bogotá",40),
            ciudad("Medellin", 70), ciudad("Barranquilla", 35)]

Plantas = [planta("Planta 1 ",80,[["Cali",5],["Bogotá",2],["Medellin",7],["Barranquilla",3]]),
            planta("Planta 2 ",30,[["Cali",3],["Bogotá",6],["Medellin",6],["Barranquilla",1]]),
            planta("Planta 3 ",60,[["Cali",6],["Bogotá",1],["Medellin",2],["Barranquilla",4]]), 
            planta("Planta 4 ",45,[["Cali",4],["Bogotá",3],["Medellin",6],["Barranquilla",6]])]

    
Colombia = problema_transporte(Plantas, Ciudades)
#Colombia.mostrar()
#Colombia.esquina_noroeste()
Colombia.costo_minimo()
Colombia.mostrar()
#%%
# http://fcaenlinea1.unam.mx/anexos/1667/1667_Anexo3 pp 16
Ciudades = [ciudad("Boston",300), ciudad("Atlanta",200),
            ciudad("Huston", 200)]


Plantas = [planta("Detroit", 100, [["Boston",5],["Atlanta",2],["Huston",3]]),
            planta("St. Lous", 300, [["Boston",8],["Atlanta",4],["Huston",3]]),
            planta("Denver", 300, [["Boston",9],["Atlanta",7],["Huston",5]])]
USA = problema_transporte(Plantas, Ciudades)
USA.mostrar()
USA.esquina_noroeste()
USA.costo_minimo()
USA.mostrar()


