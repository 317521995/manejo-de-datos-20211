# -*- coding: utf-8 -*-
"""
Created on Wed Jan  6 08:44:13 2021

@author: Lola Serrano
"""
from random import random, seed

#La función random nos genera valores uniformes en el intervalo 0 y 1
valor = random()
print(valor)
#Podemos verificar que solo nos da valores entre 0 y 1 corriendolo varias veces
for _ in range(10):
    valor = random()
    print(valor)

#Estos valores son pseudoaleatorios. Se forman con un algoritmo que utiliza una semilla
#Esta semilla la llamamos seed. Existe un modulo que nos permite establecer la semilla
#seed(1)
valor = random()
print(valor)
#Por la semilla que elegimos siempre nos va a dar el mismo número, por eso la semilla
#debe de ser elegida adecuadamente 
#%%
#Queremos ver como tener números más grandes que 1 o que salgan enteros|
b = 13
a = 7
valor = 7+random()*6
print(valor)
valor = a + random()*(b-a)
print(valor)
valor = int(a+random()*(b-a))
print(valor)
valor = (a + random()*(b-a))//1
print(valor)

#definimos una función para tener un valor aleatorio entre a y b entero
def valor_aleatorio(a,b):
    valor = int(a+random()*(b-a))
    return valor
print(valor_aleatorio(7, 13))

#una cantidad aleatoria de valores aleatorios
for i in range(valor_aleatorio(10, 15)):
    print(random())

#esta función nos regresa un numero aleatorio de valores aleatorios
def aleatorios(n,m,a,b):
    for i in range(valor_aleatorio(n,m)):
        print(a+random()*(b-a))

#Así es como la implementó el profe
def n_valores_aleatorios(a,b, N=10):
    n = valor_aleatorio(1,N)
    valores = []
    for i in range(n):
        valores.append(valor_aleatorio(a, b))
    return valores
v = n_valores_aleatorios(30, 70)
print(v)
v1 = n_valores_aleatorios(30, 70,27)
print(v1)
#%%
from random import randint
#La funcion randint(a,b) nos regresa un valor aleatorio entre a y b
print('Valores randint')
for _ in range(10):
    value = randint(3, 10)
    print( value)
#%%
#podemos usar la función gauss que nos da valores aleatorios
#con distribución gaussiana
import matplotlib.pyplot as plt
from random import gauss
l = []
for _ in range(10):
    value = gauss(0,1)
    l.append(value)
    print(value)
#Si hacemos un histograma vemos cómo se hace una campana
plt.hist(l)
#%%
#Damos una secuencia en una lista de la cual va a elegir 5 valores aleatorios
#Siempre nos sale lo mimso por definir un seed
from random import choice, seed
#seed(1)
secuencia = [i for i in range(20)]
print(secuencia)
for _ in range(5):
    seleccion = choice(secuencia)
    print(seleccion)
#%%
#Hacemos una versión pythonizada de lo anterior donde en una lista guardamos 7 valores aleatorios
l = [choice(secuencia) for i in range(7)]
print(l)
#%%
from random import sample, seed, shuffle
seed(1)
secuencua = [i for i in range(20)]
print(secuencia)
#Tomamos una muestra de secuencia de tamaño 5
#Así no tenemos que hacer la lita por comprensión, solito lo mete en una lista
subsecuencia = sample(secuencia, 5)
print(subsecuencia)

#Vamos a usar shuffle para revolver los valores que tenemos en la lista
shuffle(subsecuencia)
print(subsecuencia)
#%%
