# -*- coding: utf-8 -*-
"""
Created on Wed Nov 18 08:10:21 2020

@author: LAP-HP
"""


def mostrar(frutas):
    print('-'*20)
    for x in frutas:
        print(*x)

#Esta lista es una lista de listas que cada una tiene una fruta y otra 
#lista que dice el color y un número
frutas = [ ['pera',['verde', 15]], 
          ['manzana', ['roja', 20]],
          ['platano', ['verde', 13]],
          ['tuna', ['amarilla', 21]],
          ['mora', ['azul', 29]]] 

#Nos muestra cada elemento de la lista de forma independiente
mostrar(frutas)

#%%
#creamos una nueva lista con la función sorted
frutasOrdenadas = sorted(frutas)
mostrar(frutasOrdenadas)

#%%
#De nuevo creamos una lista nuevo con el metodo sorted pero usando la funcion lambda
#Lo que hace es ordenar con respeto al segundo elemento (o sea la lista con el color y el número)
frutasOrdenadas=sorted(frutas, key=lambda v:v[1])
mostrar(frutasOrdenadas)

#%%
#En este caso la función lambda ordena de acuerdo al segundo elemento del segundo elemento
#es decir, el número que está dentro de la lista
frutasOrdenadas=sorted(frutas, key=lambda v:v[1][1])
mostrar(frutasOrdenadas)

#%%
def mostrarD(frutas):
    print("-"*20)
    for x in frutas:
        print(x,frutas[x])

frutasd = { "pera":["verde", 15],
          "manzana":["roja",20],
          "platano":["verde",13],
          "tuna":["amarilla",21],
          "mora":["azul",39]}
mostrarD(frutasd)
#%%
#frutasOrdenadasD=sorted(frutasd)
frutasOrdenadasD={k: v for k, v in sorted(frutasd.items(), key=lambda v: v[0])}
mostrarD(frutasOrdenadasD)
#%%
#frutasOrdenadas=sorted(frutasd,key=lambda v:v[0])
frutasOrdenadasD={k: v for k, v in sorted(frutasd.items(), key=lambda v: v[1])}
mostrarD(frutasOrdenadasD)
#%%
#frutasOrdenadas=sorted(frutas,key=lambda v:v[1][1])
frutasOrdenadasD={k: v for k, v in sorted(frutasd.items(), key=lambda v: v[1][1])}
mostrarD(frutasOrdenadasD)
