# -*- coding: utf-8 -*-
"""
Created on Tue Dec  8 08:19:00 2020

@author: LAP-HP
"""

#Recordando cómo funciona programar algo de forma recursiva
#El factorial es un ejemplo básico de cómo funciona la recursividad
def factorial(n):
    if n == 0:
        return 1
    else:
        return n*factorial(n-1)
    
l_izq = [3,7,9,23,56,98]
l_der = [1,2,23,56,76,80,102]


#Esta función nos servirá para la versión recursiva de mere sort. Lo que
#hace es juntar las listas, mientras que la función de abajo las ordena.
def merge(l_izq:list, l_der:list):
    lista = []
    indiceIzq = 0
    indiceDer = 0
    while (indiceIzq < len(l_izq) and indiceDer < len(l_der)):
        valIzq = l_izq[indiceIzq]
        valDer = l_der[indiceDer]
        if valIzq <= valDer:
            lista.append(valIzq)
            indiceIzq = indiceIzq+1
        else:
            lista.append(valDer)
            indiceDer = indiceDer+1
    
    while (indiceIzq < len(l_izq)):
        valIzq = l_izq[indiceIzq]
        lista.append(valIzq)
        indiceIzq = indiceIzq + 1
    
    while (indiceDer < len(l_der)):
        valDer = l_der[indiceDer]
        lista.append(valDer)
        indiceDer = indiceDer + 1
    
    return lista

print(merge(l_izq, l_der))

#En esta otra versión utilizamos recursividad pero llamando también a la funcion
#merge que se definió arriba
def merge_sort(lista:list):
    if len(lista) <= 1:
        return lista
    else:     
        indiceMedio = len(lista)//2
        lizq = lista[:indiceMedio]
        lder = lista[indiceMedio:]
        lizqOrdenada = merge_sort(lizq)
        lderOrdenada = merge_sort(lder)
        listaOrdenada = merge(lizqOrdenada,lderOrdenada)
        return listaOrdenada
print(merge_sort())