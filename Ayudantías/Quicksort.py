# -*- coding: utf-8 -*-
"""
Created on Tue Jan 12 08:13:14 2021

@author: Lola
"""

#Estamos haciendo el quicksort de forma recursiva. Este utiliza la función particiona
#definida abajo y también se llama a sí mimsa (porque es recursiva)
def quicksortR(array, l, h):
    if l < h:
        posicionPivote = particiona(array, l, h)
        quicksortR(array, l, posicionPivote-1)
        quicksortR(array, posicionPivote+1, h)

#Este es el algoritmo quicksort iterativo
def quicksortI(array):
    stack = []
    l = 0
    h = len(array)-1
    stack.append((l,h))
    while stack:
        l,h = stack.pop()
        posicionPivote = particiona(array, l, h)
        if l < posicionPivote-1:
            stack.append((l,posicionPivote-1))
        
        if h > posicionPivote+1:
            stack.append((posicionPivote+1,h))

def particiona(array, l, h):
    pivote = array[h]
    i = 0
    j = h-1
    while i <= j:
        #En el caso que valor array[i] > pivote y array[j] < pivote
        #Se hace el swap y se aumenta i y se decrementa j
        if array[i] > pivote and array[j] < pivote :
            temp = array[i]
            array[i] = array[j]
            array[j] = temp
            i = i+1
            j = j-1
            
        #En el caso que valor array[i] > pivote y array[j] > pivote
        #Se decrementa j
        if array[i] > pivote and array[j] > pivote :
            j = j-1
        
        #En el caso que valor array[i] < pivote y array[j] < pivote
        #Se aumenta i
        if array[i] < pivote and array[j] < pivote :
            j = j+1
        
        #En el caso que valor array[i] < pivote y array[j] > pivote
        #Se aumenta i y se decrementa j
        if array[i] < pivote and array[j] > pivote :
            i = i+1
            j = j-1
            
    temp = array[i]
    array[i] = pivote
    array[h] = temp
    return i

arr = [5,3,7,6,2,1,4]
#Probamos las funciones con el array
print(quicksortR(arr, 0, len(arr)-1))
print(quicksortI(arr))
