# -*- coding: utf-8 -*-
"""
Created on Tue Feb  2 08:27:01 2021
"""
import random
import simpy
#Este es un ejemplo que nosotros haremos sobre una taquilla en un cine
def persona(env, name, taquilla, colaTaquilla, tiempo_inicio, niños, tiendita):
    yield env.timeout(tiempo_inicio)
    try:
        if niños:
            print("La persona "+name+" llegó a la fila de la taquilla en el momento "+str(env.now)+" con niños.")
        else:
            print("La persona "+name+" llegó a la fila de la taquilla en el momento "+str(env.now))
        colaTaquillaRequest = colaTaquilla.request()
        yield colaTaquillaRequest
        taquillaRequest = taquilla.request()
        yield taquillaRequest
        colaTaquilla.release(colaTaquillaRequest)
        tiempoAtenciónTaquilla = random.randrange(1, 10)
        tiempoTiendita = random.randrange(0,4)
        if niños:
            tiempoAtenciónTaquilla = tiempoAtenciónTaquilla*2
            tiempoTiendita = tiempoTiendita*2
        print("La persona "+name+" empezó a ser atendida en la taquilla en el momento "+str(env.now))
        yield env.timeout(tiempoAtenciónTaquilla)
        print("La persona "+name+" terminó de ser atendida en la taquilla en el momento "+str(env.now))
        print("La taquilla se tardó "+str(tiempoAtenciónTaquilla)+" en atender al cliente "+name)
        taquilla.release(taquillaRequest)

        #Proceso: La persona pasa a la tienda. Algunas personas pasan a la tiendita y otras no.
        if tiempoTiendita != 0:
            tienditaRequest = tiendita.request()
            yield tienditaRequest
            print("La persona " +name+ " empezó a ser atendida en la tiendita en el momento " +str(env.now) )
            yield env.timeout(tiempoTiendita)
            print("La persona " +name+ " terminó de ser atendida en la tiendita en el momento " +str(env.now) )
            tiendita.release(tienditaRequest)

    except simpy.Interrupt:
        print ("La persona "+name+" fue mandada a su casa en el momento "+str(env.now))
        colaTaquilla.release(colaTaquillaRequest)
        taquilla.release(taquillaRequest)

def vigilante(env, taquilla, colaTaquilla):
    yield env.timeout(20)
    while True:
        for user in colaTaquilla.users:
            if user.proc.is_alive:
                user.proc.interrupt()
        for user in taquilla.users:
            if user.proc.is_alive:
                user.proc.interrupt()
        
        yield env.timeout(1)

#Vamos a hacer una nueva función para simular una camara en la taquilla que se activa de 
#manera aleatoria
def camaraTaquilla(env, taquilla):
    while True:
        yield env.timeout(5)
        print('En la taquilla tenemos '+str(colaTaquilla.count)+' personas formadas')

#Probamos las funciones que se hicieron
env = simpy.Environment()
colaTaquilla = simpy.Resource(env, capacity=100)
taquilla = simpy.Resource(env, capacity=3)
tiendita = simpy.Resource(env, capacity=2)

for i in range(20):
    tiempo_espera = random.randrange(0,10)
    tieneNinos = random.randrange(0,10)
    ninos = False
    if tieneNinos == 1:
        ninos = True
    env.process(persona(env, "Persona %d" %i, taquilla, colaTaquilla, tiempo_espera, ninos, tiendita))
env.process(camaraTaquilla(env, colaTaquilla))
env.run(until=100)