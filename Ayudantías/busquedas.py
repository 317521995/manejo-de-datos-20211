# -*- coding: utf-8 -*-
"""
Created on Tue Jan 19 09:02:20 2021

@author: LAP-HP
"""
#Antes de esto estuvimos viendo sobre el stack y cómo funciona y porqué
#con recursividad se llena más

#Queremos saber si de un array hay un elemento que nosotros queremos
#Vamos a iterar sobre los elementos a ver si hay coincidencias
def BusquedaSecuencial(array, n):
    for indice in range(len(array)):
        if n == array[indice]:
            return indice
#Sirve para un array pequeño: ejemplo buscando 1
array = [5,8,3,1,3]
indice = BusquedaSecuencial(array, 3)
print(indice)
print()

#Esto sirve para cuando tenemos un array muy grande, donde no se tiene que ir elemento por elemento
#En este vamos partiendo el array en arrays más pequeños y en estos buscar el elemento
def BusquedaBinaria(array,l,h,x):
    #La mitad del array es med, lo usamos para poder dividir la lista
    med = (1+h)//2
    print(med)
    indiceEncontrado = None
    if array[med] == x or l == h or l > h:
        if array[med] == x:
            #indiceEncontrado = med
            return med
        else:
            return None
    else:
        if (array[med] < x):
            #print('mayor')
            indiceEncontrado = BusquedaBinaria(array, med+1, h, x)
            return indiceEncontrado
        if (array[med] > x):
            #print('menor')
            indiceEncontrado = BusquedaBinaria(array, l, med-1, x)
            return indiceEncontrado
        
#probamos la función de busquedda binaria
array = [5,3,7,6,6,1,4]
print(BusquedaBinaria(array, 0, len(array)-1, 6))
print()

#Este es muy parecido pero calcula l y h
def BusquedaBinaria2(array,x):
    #l lo fijamos como 0 y h es el tamaño del array
    l = 0
    h = len(array)-1
    #La mitad del array es med, lo usamos para poder dividir la lista
    med = (1+h)//2
    print(med)
    indiceEncontrado = None
    if array[med] == x or l == h or l > h:
        if array[med] == x:
            #indiceEncontrado = med
            return med
        else:
            return None
    else:
        if (array[med] < x):
            #print('mayor')
            indiceEncontrado = BusquedaBinaria(array, med+1, h, x)
            return indiceEncontrado
        if (array[med] > x):
            #print('menor')
            indiceEncontrado = BusquedaBinaria(array, l, med-1, x)
            return indiceEncontrado
        
#probamos la función. Sale lo mismo que con la función anterior
array = [5,3,7,6,6,1,4]
print(BusquedaBinaria2(array, 6))