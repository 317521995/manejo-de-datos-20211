# -*- coding: utf-8 -*-
"""
Created on Tue Jan  5 08:29:52 2021

@author: Lola Serrano
"""
#Esta es la versión recursiva del algoritmo de ordenamiento insertion sort

def insertionSortRecursivo(arr, n):
    if n <= 1:
        return 
    #Se vuelve a llamar a si misma para hacer insertion sort con un array cada vez más pequeño hasta
    #que sea de tamaño 1.
    
    insertionSortRecursivo(arr, n-1)
    ultimo = arr[n-1]
    j = n-2
    
    while(j >= 0 and arr[j]>ultimo):
        arr[j+1] = arr[j]
        j = j-1
    arr[j+1] = ultimo

#Lo probamos con un array

arr = [2,8,5,3,9,4]
n1 = len(arr)
insertionSortRecursivo(arr, n1)
print('El arreglo ordenado es: \n')
for i in range(n1):
    print(arr[i], end = '')
    
