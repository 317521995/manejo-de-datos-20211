# -*- coding: utf-8 -*-
"""
Created on Tue Nov  3 07:25:53 2020

@author: LAP-HP
"""
import math

#Definimos primero a la clase vertice. Los atributos de los objetos
#vertice son su número o nombre mas su valor (o sea si fue visitado o no)
class vertice():
    def __init__(self, indentificador, valor=0):
        self.set_identificador(indentificador)
        self.set_valor(valor)
        
    def set_identificador(self, identificador):
        self.__identificador = identificador
    
    def get_identificador(self):
        return self.__identificador
    
    def set_valor(self, valor):
        self.__valor = valor
        
    def get_valor(self):
        return self.__valor
    
    def __str__(self):
        return 'Vertice:'+str(self.get_identificador())+' Valor:'+str(self.get_valor())
    
    def __repr__(self):
        return self.__str__()
ver1 = vertice(1,1)
ver2 = vertice(2)
print(ver1)
print(ver2)

#Luego la clase arista. Los atributos de estos objetos son los dos vertices 
#que tiene como extremos y el peso de esa arista. El peso nos importa a la hora del algoritmo
class arista():
    def __init__ (self, id_vertice_a, id_vertice_b, peso):
        self.set_id_vertice_a(id_vertice_a)
        self.set_id_vertice_b(id_vertice_b)
        self.set_peso(peso)
        
    def set_id_vertice_a(self, id_vertice_a):
        self.__id_vertice_a = id_vertice_a 
    
    def get_id_vertice_a(self):
        return self.__id_vertice_a
    
    def set_id_vertice_b(self, id_vertice_b):
        self.__id_vertice_b = id_vertice_b
    
    def get_id_vertice_b(self):
        return self.__id_vertice_b
    
    def set_peso(self, peso):
        self.__peso = peso
    
    def get_peso(self):
        return self.__peso
    
    def __str__(self):
        return 'Arista:'+str(self.get_id_vertice_a())+'-'+str(self.get_id_vertice_b())+' Peso:'+str(self.get_peso())
    
    def __repr__(self):
        return self.__str__()     
ari1 = arista(1,2,43)
print(ari1)

#Definimos la clase gráfica que usa listas de vertices y de aristas
class grafica():
    
    def __init__(self, vertices, aristas):
         self.set_vertices(vertices)
         self.set_aristas(aristas)
         
    def set_vertices(self, vertices):
        self.__vertices = vertices
    
    def get_vertices(self):
        return self.__vertices
    
    def set_aristas(self, aristas):
        self.__aristas = aristas
        
    def get_aristas(self):
        return self.__aristas
    
    #Este metodo es para el algoritmo
    def get_vecinosDe(self, id_vertice):
        listaVecinos = []
        aristasgraf = self.get_aristas()
        for arista in aristasgraf:
            if id_vertice == arista.get_id_vertice_a():
                listaVecinos.append(arista.get_id_vertice_b())
            if id_vertice == arista.get_id_vertice_b():
                listaVecinos.append(arista.get_id_vertice_a())
        return listaVecinos
        
    def get_pesoArista(self, id_vertice_a, id_vertice_b):
        aristasgraf = self.get_aristas()
        for arista in aristasgraf:
            if id_vertice_a == arista.get_id_vertice_a() and id_vertice_b == arista.get_id_vertice_b():
                return arista.get_peso()
            if id_vertice_a == arista.get_id_vertice_b() and id_vertice_b == arista.get_id_vertice_a():
                return arista.get_peso()
            
    def get_valorVertice(self, id_vertice):
        verticesgraf = self.get_vertices()
        for vertice in verticesgraf:
            if id_vertice == vertice.get_identificador():
                return vertice.get_valor()
        return None
    
    def set_valorVertice(self):
        cadena = ''
        for vertice in self.get_vertices():
            cadena += str(vertice)+ '\n'
        for arista in self.get_aristas():
            cadena += str(arista) + '\n'
        return cadena
    
    def __str__(self):
        cadena = ''
        for arista in self.get_aristas():
            cadena += str(arista)+ '\n'
        for vertice in self.get_vertices():
            cadena += str(vertice)+ '\n'
        return cadena

class dijkstra():
    def __init__(self, grafica, id_vertice_inicial, id_nodo_destino):
        self.set_grafica(grafica)
        self.set_id_vertice_inicial(id_vertice_inical)
        self.set_id_nodo_destino(id_nodo_destino)
        self.set_id:vertice_actual(id_vertice_actual)
        lista_vertices_no_visitados = []
        vertices_de_la_grafica = self.get_grafica().get_vertices()
        for vertice in vertices_de_la_grafica:
            lista_vertices_no_visitados.append(vertice.het_identificador())
        self.set_vertice_no_visitados(lista_vertices_no_visitados)
        self.set_vertices_visitados([])
        
        for vertice in vertices_de_la_grafica:
            id_de_este_vertice = vertice.get_identificador()
            if id_de_este_vertice == id_vertice_inicial:
                self.get_grafica().set_valorVertice(id_de_este_vertice,0)
            else:
                self.get_grafica().set_valorVertice(id_de_este_vertice,math.inf)
        
    def set_grafica(self, grafica):
        self.__grafica = grafica
    
    def get_grafica():
        return self.__grafica
    
    def set_id_vertice_inicial(self, id_vertice_inicial):
        self.__id_vertice_inicial = id_vertice_inicial
      
    def get_id_vertice_inicial(self):
        return self.__id_vertice_inicial
    
    def set_id_vertice_destino(self, id_vertice_destino):
        self.__id_vertice_destino = id_vertice_destino
    
    def get_id_vertice_destino(self):
        re
        
    def set_vertices_no_visitados(self vertices_no_visitados):
        return self.__vertices.vertice   
    
    def __repr_(self):
    
    def run_step(self):
        grafNodoActual_id = self.get_id_vertice_actual()
        vecinosNodoActual = self.get_grafica().get_vecinosDe(grafNodoActual_id)
        nodosNoVisitados = self.get_vertices_no_visitados()
        vecinosNoVisitados = []
        print(grafNodoActual_id)
        print(vecinosNodoActual)
        print(nodosNoVisitados)
        for id_vertice in vecinosNodoActual:
            if id_vertice in nodosNoVisitados:
                vecinosNoVisitado.append(id_vertice)
        print(vecinosNoVisitados)
        
        for id_vertice in vecinosNoVisitados:
            valorVerticeActual = self.get_grafica().get_valorVertice(grafNodoActual_id)
            pesoAristaQueConecta = self.get_grafica().get_pesoArista(grafNodoActual_id, id_vertice)
            pesoNuevo = valorVerticeActual+pesoAristaQueConecta
            pesoViejo = self.get_grafica().get_valorVertice(id_vertice)
            if pesoNuevo < pesoViejo:
                self.get_grafica().set_valorVertice(id_vertice, pesoNuevo)
            
        nodosVisitados = self.get_vertices_visitados()
        nodosVisitados.append(grafNodoActual_id)
        nodosNoVisitados.remove(grafNodoActual_id)
        self.set_vertces_no_visitados(nodosNoVisitados)
        self.set_vertices_visitados(nodosVisitados)
    
    def verifica_termino(self):
        nodosNoVisitados = self.get_vertices_no_visitados()
        nodosVisitados = self.get_vertices_visitados()
        grafNodoDestino_id = self.get_id_vertice_destino()
        if grafNodoDestino_id in nodosVisitados:
            return True
        algunoNoEsInfinito = False
        for id_vertice in nodosNoVisitados:
            pesoActualVertice = self.get_grafica().get_valorVertice(id_vertice)
            if pesoActualVertice != math.inf:
                algunoNoEsInfinito = True
        return not algunoNoEsInfinito
    
    def actualiza_nodo_actual(self):
        nodosNoVisitados = self.get_vertices_no_visitados()
        verticeMenorPeso = None
        menorPeso = None
        for id_vertice in nodosNoVisitados:
            if verticeMenorPeso == None:
                verticeMenorPeso = id_vertice()
                menorPeso = pesoActualVertice
        self.set_id_vertice_actual(verticeMenorPeso)
        
    def recuperaCamino(self, vertice_objetivo):
        llegamosAlVerticeInicial = False
        verticesCamino = []
      
        
        while(not llegamosAlVerticeInicial):
            if vertice_objetivo == llegamosAlVerticeInicial:
                llegamosAlVerticeInicial = True
                break
            
            
            pesoVertice = self.get_grafica().get_valorVertice(vertice_objetivo)
            vecinos = self.get_grafica().get_vecinosDe(vertice_objetivo)
            verticesCamino.append(vertice_objetivo)
        for vertice_id in vecinos:
            pesoVerticeVecino = self.get_grafica().get_valorVertice(vertice_id)
            pesoAristaConecta = self.get_grafica().get_pesoArista(vertice_objetivo, vertice_id)
            if pesoVertice == pesoVerticeVecino
    
    def run(self)
    elalgoritmotermino = dijks.verifica_termino()
    print(self)
    while(elalgoritmotermino == False):
        self.run_step()
        elalgoritmotermino = dijks.verifica_termino()
        print(self)
        if (not elalgoritmotermino):
            self.actualiza_nodo_actual()
        
        
     
        
        
        
#Vamos a hacer una lista de vertices y una de aristas para la grafica de Dis¿jstra que vimos en clase
listaVertices = []
listaAristas = []

#Los que dice math.inf es porque aun no son visitados ni hemos checado su peso acumulado
vertice1 = vertice(1,0)
vertice2 = vertice(2, math.inf )
vertice3 = vertice(3, math.inf)
#Los metemos a la lista de vertices
listaVertices.append(vertice1)
listaVertices.append(vertice2)
listaVertices.append(vertice3)

#Para la arista1 quiere decir que los extremos son los vertices 1 y 2 y que 
#el peso de esa arista es 3. Y así para las demás
arista1 = arista(1,2,3)
arista2 = arista(2,3,2)
arista3 = arista(1,3,4)
#Metemos las aristas a la lista de aristas
listaAristas.append(arista1)
listaAristas.append(arista2)
listaAristas.append(arista3)

#Con esas dos listas ya podemos definir al objeto graf
graf = grafica(listaVertices, listaAristas)
#Lo imprimimos
print(graf)
print(graf.get_aristas())
print(graf.get_vertices())
print(listaAristas)
print()

pesoArista12 = graf.get_pesoArista(1, 2)
pesoArista21 = graf.get_pesoArista(2,1)
print(pesoArista12)
print(pesoArista21)