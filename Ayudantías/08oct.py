# -*- coding: utf-8 -*-
"""
Created on Fri Feb  5 14:03:18 2021

@author: LAP-HP
"""


class Persona(object):
    def __init__(self, nombre):
        self.nombre = nombre
    def getName(self):
        return self.nombre
    def esEmpleado(self): #Es persona pero no es empleado
        return False
    
class Empleado(Persona): #Subclase de Persona
    def esEmpleado(self):
        return True

emp1 = Persona("Fulanito")
print(emp1.getName(), emp1.esEmpleado())

emp2 = Empleado("Perenganito")
print(emp2.getName(), emp2.esEmpleado())