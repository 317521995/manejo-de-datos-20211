# -*- coding: utf-8 -*-
"""
Created on Thu Dec  3 08:15:01 2020

@author: Lola Serrano
"""

#Esta es la versión iterativa del insertion sort
def insertionSort(arr):
    for i in range(1, len(arr)):
        key = arr[i]
        j = i-1
        while j>=0 and key < arr[j]:
            arr[j+1]=arr[j]
            j -= 1
        arr[j+1] = key
        
arr = [2,8,5,3,9,4]
insertionSort(arr)
print('Arreglo ordenado:')
for i in range(len(arr)):
    print('%d' %arr[i])
#En el documento insertionrec.py está la versión recursiva de este algoritmo de ordenamiento 
 
    