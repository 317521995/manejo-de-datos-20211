# -*- coding: utf-8 -*-
"""
Created on Tue Jan 26 08:16:40 2021

@author: LAP-HP
"""
import simpy 
#Vamos a aprender a usar un nuevo paquete que sirve para simular eventos discretos
#Toda la documentación está en el siguiente enlace
#https://simpy.readthedocs.io/en/latest/simpy_intro/index.html

def car(env):
    while True:
        print('Start parking at %d' %env.now)
        parking_duration = 5
        yield env.timeout(parking_duration)
        
        print('Start parking at %d' %env.now)
        trip_duration = 2
        yield env.timeout(trip_duration)

env = simpy.Environment()
env.process(car(env))
env.run(until=15)


        
