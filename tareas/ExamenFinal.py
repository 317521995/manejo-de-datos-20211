# -*- coding: utf-8 -*-
"""
Created on Sat Jan 23 17:02:36 2021

@author: Lola Serrano
"""
import numpy as np
import random
'''
Examen Final Manejo de Datos
Lola Serrano Carrasco - No. de cuenta: 317521995
#%%
p1: ¿Qué es una clase?

r1: Una clase es un tipo de dato con el cual creamos objetos. Estos objetos tienen ciertos atributos que
nosotros les damos. Para poder darle esos atributos o características se usa el método constructor __init__.
Podemos hacer que nuestro objeto sea representado con una cadena a nuestra conveniencia. Para eso de usa
el método __str__.

Algo particular de las clases es que las funciones son llamadas métodos. Nosotros las podemos definir
de acuerdo a lo que queremos que haga un objeto o cosas que puedan pasar entre objetos. Hay algunos métodos
llamados dunder que llevan dos guiones bajos al principio y al final. Estos ya existen pero nosotros los podemos
usar como más nos convenga de acuerdo al objeto que estamos haciendo. Por ejemplo el método __add__ 
que suma dos objetos. Si nuestro objeto fueran polinomios lo definimos como que suma términos semejantes entre
los polinomios pero si fueran personas lo podriamos definir como una cadena que diga 'persona1 y persona2'.

También se pueden hacer métodos propios que no sean dunder. Por ejemplo en el de los polinomios un método
que encuentre la derivada o la integral de ese polinomio.

#%%
p2: Implementa una clase llamada 'mascotas', ¿qué atributos le darías a un objeto?
Implementa dos métodos, aparte del __init__ y __str__''


r2:'''
class Mascotas():   
    #Primero debemos de hacer el método constructor con los diferentes atributos que vamos a escoger
    def __init__(self, nombre, especie, peso, edad, sexo, dueño):
        self.nom = nombre
        self.esp = especie
        self.kg = peso
        self.años = edad
        self.MF = sexo
        self.dueño = dueño

    def __str__(self):
        return "La mascota {} es de la especie {}. Tiene {} años y pesa {} kilos. Es un(a) {} y su dueñx es {}". format(self.nom, self.esp, self.años, self.kg, self.MF, self.dueño)
    
    #Este método almacena información sobre la cartillad de vacunación del gato. La guarda
    #En un diccionario y luego regresa este diccionario
    def Cartilla(self):
        cartilla = {}
        Rabia = str(input('¿Tu mascota está vacunada contra la rabia. Escribe si o no \n'))
        if Rabia == 'si':
            cartilla['Rabia'] = True
        elif Rabia == 'no':
            cartilla['Rabia'] = False
        
        Parvovirus = str(input('¿Tu mascota está vacunada contra el parvovirus? \n'))
        if Parvovirus == 'si':
            cartilla['Parvovirus'] = True
        elif Rabia == 'no':
            cartilla['Parvovirus'] = False

        Otra = str(input('¿Tu mascota tiene alguna otra vacuna? Si sí ingresa únicamente el nombre. Si no, escribe no \n'))
        if Otra == 'no':
            cartilla = cartilla
        else:
            cartilla[Otra] = True
        return cartilla
    
    #Este segundo método nos dice si nuestra mascota es pequeña, mediana o grande
    #Solamente está d.efinida para perros y gatos
    def Tamaño(self):
        peso = int(self.kg)
        if self.esp == 'Perro':
            if peso <= 10:
                return 'Tu perro es pequeño'
            elif peso > 10 and peso <= 30:
                return 'Tu perro es mediano'
            else:
                return 'Tu perro es grande'
        elif self.esp == 'Gato':
            if peso < 4:
                return 'Tu gato es chico'
            elif peso >=4 and peso < 7:
                return 'Tu gato es mediano'
            else:
                return 'Tu gato es grande'  
            
                
#Probando con la información de mi gatita ambos métodos
m1 = Mascotas('Pepa','Gato','3','5','Hembra','Lola Serrano')
print(m1)
print(m1.Cartilla())
print(m1.Tamaño())
print()
'''
#%%
p3: ¿Qué son los setters y los getters?

r3: En inglés set significa fijar. Entonces casi literal los setters son
métodos que fijan el valor de un atributo. Get significa agarrar o tomar. Entonces
los getters son métodos que toman o rescatan el valor de un atributo.

#%%
p4: ¿Cuál es la diferencia entre atributo público y privado?

r4: Los atributos privados sólo se puede acceder a ellos desde la implementación de la clase, 
con los setters y getters. En cambio, para los públicos se puede acceder a ellos cuando son declarados.

#%%
p5: ¿De qué trata el problema del transporte? 

r5: El problema del transporte trata de encontrar la mejor manera de distribuir algún
producto a un número de lugares o ciudades. Nosotros lo vimos como 4 plantas que 
deben distribuir a 4 ciudades diferentes. Podemos acomodar esta información en una tabla 
donde las filas son las plantas y las columnas las ciudades. En las celdas que quedan 
como intersección va el costo de distribuir de esa planta a esa ciudad. 

Cada ciudad tiene una demanda que cubrir y cada planta tiene una oferta, que es el 
máximo que puede repartir. El objetivo es encontrar la manera de distribuir para cubrir
todas las demandas con el menor costo posible

OBS: Puede pasar que si una planta tiene oferta de 50 y una ciudad tiene demanda de 40 y otra
de 10 se reparta la oferta entre dos ciudades. Igual una ciudad puede cubrir su demanda con
lo que le ofrece más de una planta; no debe ser exclusivamente todo para una y ya. 

#%%
p6: ¿Qué significa IPDB y a grandes rasgos para qué sirve?

r6: Sifnifica Interactive Python DeBugger. Sirve para encontrar errores en los scripts.
Esto es práctico para códigos muy largos o complejos donde es difícil encontrar el error
manualmente. El IPDB tiene  puntos de ruptura para poder encontrar el error hasta ese 
punto, sin tener que buscar el todo el script.
    
#%%
p7: Describe el algoritmo de Disjktra. 

r7: El algoritmo de Dijkstra usa una gráfica con aristas con pesos. Los vértices están númerados
Se busca ir de un nodo inicial a otro por medio de las aristas menos pesadas: el camino que pese menos

Preliminares: Todos los nodos empiezan con una etiqueta N que representa no visitado. También tienen
una etiqueta de infinito porque aun no conocemos su peso. La única que no tiene ese infinito es el nodo
inicial que tiene un cero porque el peso de ir de si misma a si misma es cero. En el paso uno el nodo
actual es el nodo inicial, pero este nodo actual va a ir cambiando

Paso 1: Estando en el nodo inicial se checan las aristas salientes y sus pesos. Se elige la arista con
menor peso. Se va primero al nodo que se conecta por medio de la arista con menos peso. Quitamos el infinito y ponemos
el peso de esa arista. Luego visitamos los demás nodos vecinos del nodo inicial que es el actual. Los vamos visitando 
de acuerdo al peso de sus aristas en orden menor a mayor. Hacemos lo mismo de cambiar el infinito por el peso.

Paso 2: Nos fijamos en los nodos vecinos y el que tenga el menor peso acumulado es el nuevo nodo actual. Luego nos fijamos 
en los vecinos de este nodo actual repitiendo el procedimiento anterior. Si alguno de esos vecinos ya tenía un peso
a ese le sumamos el peso de las aristas que los conectan. Al acabar con los vecinos vemos cuál tiene menor
peso y ese será el nuevo actual, y el ciclo se repite. Si un nodo ya fue visitado y no es el actual le quitamos
tanto la etiqueta de visitado como la de no visitado.

Paso 3: Una vez que repetimos este procedimiento con todos los nodos de la gráfica seguimos el camino
de pesos más pequeños que unan los vértices que queremos y así termina el algoritmo.

FIN

#%%
p8: Escoge uno de los algoritmos de ordenamiento vistos en clase y describelo detalladamente

r8: Voy a explicar MERGE SORT. Este es conocido como el algoritmo de divide y vencerás. Se divide a la lista en
pequeñas listas para poder solucionar más fácil el problema. Es por esto que usa recursividad.

Supongamos que tenemos una lista de números a ordenar. Esta la vamos dividir en pequeñas sublistas hasta 
tener listas de un solo elemento
    lista = [7,3,1,4,5,2]
        [7,3,1]      [4,5,2]    
     [7,3]   [1]   [4,5]   [2]
   [7] [3]  [1]  [4] [5]   [2] 
   
Una vez que tenemos todas las sublistas vamos a comparar de dos en dos y ordenarlas de menor a mayor
Hay que guardarlas en una lista que contenga sólo 2
   [7] [3]  [1] [4]  [5] [2] 
    [3,7]    [1,4]    [2,5]

#Luego de estas parejas volvemos a comparar pareja por pareja ordenando de menor a mayor
   [7] [3]  [1] [4]  [5] [2] 
    [3,7]    [1,4]    [2,5]   
      [1,3,4,7]      [2,5]
          [1,2,3,4,5,7]


#%%
p9: Menciona dos aplicaciones de los algoritmos de ordenamiento

r9: Una aplicación puede ser en el problema del transporte, usando la solución del costo mínimo. En esa solución
debíamos de ordenar los costos de menor a mayor y empezar por la planta-ciudad con el menor costo. Para eso
se necesitó de un algoritmo de ordenamiento que pusiera de menor a mayor los costos.

Otra aplicación puede ser en un inventario de ropa: ordenar de mayor a menor de acuerdo a al tiempo que llevan en la 
tienda o en el almacen. Así, las que llevan más tiempo saben que son las que deben de poner en la sección de 
descuentos de la tienda.

#%%
p10: ¿Cuál es la diferencia de recursividad e iteratividad? Menciona también las ventajas y desventajas de cada uno

r10: La recursividad necesita de un caso base el cuál le indicamos cómo se debe resolver. Para resolver un problema
recursivo va a llamarse a si mismo a la función varias veces hasta llegar a este caso base y a partir de este
resolver el problema. Una ventaja es que hay muchos problemas que es más práctico resolverlos con recursividad. 
Sin embargo, si es un problema muy grande o que se tiene que llamar a si misma muchas veces se va a alentar exponencialmente

La iteratividad se va fijando en cada uno de los elementos de una lista, o en cada entero de 1 a n, o en cada carácter de 
una cadena, etc. Va iterando sobre cada uno y con cada uno va haciendo algo. En mi opinión, muchas veces
es más fácil de entender y de implementar. También porque estamos más acostumbrados a este tipo de implementaciones.
Además es más fácil ir comprobando paso a paso que estamos haciendo el código correcto. La desventaja es que hay 
algunos problemas que se pueden resolver fácil en dos líneas usando recursividad y la iteratividad nos tomaría
más tiempo de escribir y de implementar (pero ahí sí depende del caso).

#%%
p11: Menciona un problema que se pueda resolver con recursividad e implementa una solución. 
Haz lo mismo con uno que se pueda resolver con iteratividad.

r11: PROBLEMA CON RECURSIVIDAD: Un ejemplo de problema que usa recursividad es el encontrar
el determinante de una matriz. Este se encuentra con los determinantes de las submatrices
Los determinantes de las submatrices se encuentran a la vez con los determinantes de sus 
propias submatrices. 

Este proceso se repite hasta llegar al caso base con submatrices 
de tamaño 2x2, ya que estas sí se sabe cómo encontrar el determinante: multiplicando la diagonal
y restando el producto de la diagonal inversa
'''
#Esta función la hice como una tarea de otro curso entonces ya tenía mi ejemplo de recursividad :)
def Determinante(M):
    A = np.matrix.copy(M)
    n,n = A.shape
    #Primero encontramos las submatrices de la matriz
    #Eso se hace quitando la primera fila e iterando sobre las columnas para quitar una por una
    subm = A[1:,:]
    #Las submatrices las guardamos en una lista
    lista_sub = []
    for i in range(n):
        lista_sub.append(np.delete(subm, i, axis = 1))
    #Así se resuelven las matrices cuando n=2, o sea las de 2x2. Esre es el caso base
    det_2 = 0
    if n==2:
        det_2 += A[0,0]*A[1,1]- A[0,1]*A[1,0]
        return det_2
    
    #Cualquier otro caso es cuando n > 2. Necesitamos la función recursiva
    else:
        #Hacemos una lista donde se guardan los sumandos o cofactores. 
        sumandos_lista = []
        for i in range(n):
            sumandos_lista.append((-1)**(i)*A[0,i]*(Determinante(lista_sub[i])))
            #Sumamos los cofactores para tener el determinante
            det = sum(sumandos_lista)
        return det
'''
Lo probamos con una matriz:
'''
M = np.array([[2,3,6,1],[0,2,3,5],[9,12,43,2],[9,8,7,6]])
print('Probando algoritmo recursivo de determinante: \n M =',M, ' \n el determinante es',Determinante(M))
print()
'''
PROBLEMA CON ITERATIVIDAD: Un ejemplo clásico de recursividad es el del factorial, el cual vimos en clase
Pero este se puede resolver también con iteratividad. Esta función recibe como parámetro un natural n
al cual le queremos calcular el factorial

Para eso se usa una variable auxiliar que empieza en 1 y a la cual se le iran multiplicando
los valores menores o igual a número del que queremos el factorial
'''
def Factorial_Recursivo(n):
    #Empezamos con nuestra variable auxiliar en 1
    factorial = 1
    #Hacemos un ciclo que itera con todos los enteros positivos menores o iguales a n
    for i in range(1,n+1):
        #Por cada uno de estos valores que vamos iterando lo vamos a multiplicar por la variable auxiliar
        factorial = factorial*i
    return factorial
        
'''Lo probamos con algunos valores'''
print('Probando al iterativo para algunos valores: \n Para n = 8, n!=',Factorial_Recursivo(8), '\n Para n = 0, n!=',Factorial_Recursivo(0))
print()
#%%
'''
p12: ¿Porque se dice que los valores aleatorios de Python son pseudoaleatorios?
¿Cómo son hechos?

r12: En realidad la compu no elige un valor aleatorio que le guste, los forma con 
una semilla o seed. Por eso son pseudoaleatorios porque sí están determinados por esta 
semilla. pero como esta va cambiando los números difícilmente se repiten

#%%
p13: Utiliza 3 de los métodos vistos del paquete random y explicalos

r13:  Uno de los que vimos fue choice, que a partir de un array elige un valor al azar.
No tienen que ser necesariamente valores numéricos.
'''
lista_choice = [0,1,'hola',9,[],10, 'cadena']
prueba_choice = random.choice(lista_choice)
print('El valor aleatorio que se eligió de la lista fue:' , prueba_choice)

'''
Un segundo método de la biblioteca random visto en clase fue randint. Normalmente si
le pedimos un número aleatorio nos dará uno flotante, pero con este nos regresa un entero.
Es importante especificar el primer valor que puede tomar y el último que puede tomar
(o sea el rango de valores de donde va a escoger)
'''
#En este caso puede elegir de 2 a 10, porque el 11 no cuenta
prueba_randint = random.randint(2,11)
print('El valor entero aleatorio fue:', prueba_randint)
'''
Otro de los que vimos fue shuffle. Supongamos que tenemos una bolsita y se saca el elemento
que está hasta arriba y luego se regresa, y ese queda hasta arriba. Luego se saca otro. 
Como no queremos sacar el mismo para siempre se usa la función suffle que
revuelve a los elementos de un array, o de la bolsita, y los pone en posiciones 
diferentes aleatorias.
'''
antes_shuffle = [0,1,2,3,4,5,6,7,8,9]
print('La lista antes de mezclarse:', antes_shuffle)
despues_shuffle = random.shuffle(antes_shuffle)
print('La lista después de mezclarse aleatoreamente:',antes_shuffle)