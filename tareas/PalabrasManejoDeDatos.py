# -*- coding: utf-8 -*-
"""
Created on Sat Jan 23 19:29:38 2021

@author: Lola Serrano

Semana 1: Clases, Método, Objeto, Constructor, Python, Spiderman
Semana 2: Setter, Getter, Privado, Público, Pass, Cadena, Clases
Semana 3: Java, Python, Método, Función, Dunder
Semana 4: Help, Condicionales, If, For, Ciclo, While, Break, GitAhead, Repositorio, Matplotlib, Biblioteca
Semana 5: IDO, Algoritmo, Transporte, Esquina Noroeste, Clases,
Semana 6: Transporte, Destino, Depurar, IPDB, Punto de ruptura, Explorador de Variables, Git, Overleaf, LaTeX, Dikstra, Gráficas, Algoritmo
Semana 7: Dikstra, Vértice, Arista, Clases, Problema del Transporte
Semana 8: Problema del Transporte, Dikstra, __str__, __repr__, Alternativas, Costo Mínimo, Ordenamiento, Función Lambda, Sorted
Semana 9: Dijkstra, Ordenamiento, Sorted, Sort, PDB, IPDB, Try, Raise
Semana 10: Costo Mínimo, Sorted, Bubble sort, Problema del Transporte
Semana 11: Bubble Sort, Sorted, Iteratividad, Merge Sort, Insertion Sort, Recursividad
Semana 12: Iteratividad, Recursividad, Fibonacci, Ciclos, Merge, Condicionales	
Semana 13: Algoritmos, Ordenamiento, Análisis, Valores Aleatorios, Semilla, Random, Quick Sort, Aleatorios Enteros
Semana 14: Matrices, Clases, Aleatorio, Ciclo, Biblioteca, Ordenamiento, 
Semana 15: Ordenamiento, SQL, Base de Datos, Cliente, Servidor 
"""

#Se va a poner esta información en una lista de listas para poder poblar la tabla de la base de datos
palabras =  [[1,'Clases, Método, Objeto, Constructor, Python, Spiderman'],
             [2,'Setter, Getter, Privado, Público, Pass, Cadena, Clases'],
             [3,'Java, Python, Método, Función, Dunder'],
             [4, ' Help, Condicionales, If, For, Ciclo, While, Break, GitAhead, Repositorio, Matplotlib, Biblioteca'],
             [5, 'IDO, Algoritmo, Transporte, Esquina Noroeste, Clases'],
             [6,'Transporte, Destino, Depurar, IPDB, Punto de ruptura, Explorador de Variables, Git, Overleaf, LaTeX, Dikstra, Gráficas, Algoritmo'],
             [7,'Dikstra, Vértice, Arista, Clases, Problema del Transporte'],
             [8, 'Problema del Transporte, Dikstra, __str__, __repr__, Alternativas, Costo Mínimo, Ordenamiento, Función Lambda, Sorted'],
             [9, 'Dijkstra, Ordenamiento, Sorted, Sort, PDB, IPDB, Try, Raise'],
             [10, 'Costo Mínimo, Sorted, Bubble sort, Problema del Transporte'],
             [11, 'Bubble Sort, Sorted, Iteratividad, Merge Sort, Insertion Sort, Recursividad'],
             [12, 'Iteratividad, Recursividad, Fibonacci, Ciclos, Merge, Condicionales	'],
             [13, 'Algoritmos, Ordenamiento, Análisis, Valores Aleatorios, Semilla, Random, Quick Sort, Aleatorios Enteros'],
             [14,'Matrices, Clases, Aleatorio, Ciclo, Biblioteca, Ordenamiento' ],
             [15, 'Ordenamiento, SQL, Base de Datos, Cliente, Servidor ']]