#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 29 13:55:25 2021

@author: luis
"""
#Se explica cómo extraer información de una base de datos desde python
import datetime
import mysql.connector

#Primero hay que conectarse a una base de datos y servidor
config = {
  'user': 'scl',
  'password': '317521995',
  'host': 'mysql-17763-0.cloudclusters.net',
  'port': '17792',
  'database': 'scl_employee',
  'raise_on_warnings': True
}

cnx = mysql.connector.connect(**config)
cursor = cnx.cursor()

#Esta tupla es la información que queremos, se usa select que usamos en mysql shell
consulta = ("SELECT first_name, last_name, hire_date FROM employees ")


#Se definen dos fechas
hire_start = datetime.date(1999, 1, 1)
hire_end = datetime.date(1999, 12, 31)
# query = ("SELECT first_name, last_name, hire_date FROM employees "
#          "WHERE hire_date BETWEEN %s AND %s")
cursor.execute(consulta)

for (first_name, last_name, hire_date) in cursor:
  print("{}, {} fue contratado el {:%d %b %Y}".format(
    last_name, first_name, hire_date))

cursor.close()
cnx.close()