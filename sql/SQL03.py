#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 29 13:52:32 2021

@author: luis
"""
#Este es para meter información a la base de datos

#from __future__ import print_function
from datetime import date, datetime, timedelta
import mysql.connector



config = {
  'user': 'scl',
  'password': '317521995',
  'host': 'mysql-17763-0.cloudclusters.net',
  'port': '17792',
  'database': 'scl_employee',
  'raise_on_warnings': True
}

cnx = mysql.connector.connect(**config)
cursor = cnx.cursor()

tomorrow = datetime.now().date() + timedelta(days=1)

add_employee = ("INSERT INTO employees "
               "(first_name, last_name, hire_date, gender, birth_date) "
               "VALUES (%s, %s, %s, %s, %s)")
add_salary = ("INSERT INTO salaries "
              "(emp_no, salary, from_date, to_date) "
              "VALUES (%(emp_no)s, %(salary)s, %(from_date)s, %(to_date)s)")

data_employee = ('Geert', 'Vanderkelen', tomorrow, 'M', date(1977, 6, 14))

# Insertar un empleado nuevo
cursor.execute(add_employee, data_employee)
emp_no = cursor.lastrowid

# Insertar la información del salario
data_salary = {
  'emp_no': emp_no,
  'salary': 50000,
  'from_date': tomorrow,
  'to_date': date(9999, 1, 1),
}
cursor.execute(add_salary, data_salary)

# Asegurarse que los datos se registran en la base de datos
cnx.commit()

cursor.close()
cnx.close()