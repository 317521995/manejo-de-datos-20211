# -*- coding: utf-8 -*-
"""
Created on Mon Oct  5 08:14:03 2020

@author: LAP-HP
"""
#Haremos una clase donde los objetos son racionales
class racional():
    #vamos a implementar el metodo init que requiere de self,
    #y en este caso numerador y denominador
    #Asi empezamos a definir clases. es el metodo constructor
    def __init__(self, numerador, denominador):
        self.p = numerador
        self.q = denominador
    #el metodo str nos regresa una cadena que representa al objeto
    def __str__(self):
        return "{}/{}".format(self.p,self.q)
    #Este metodo nos regresa el producto del primer racional con el segundo
    def __mul__(self, r1):
        return racional(self.p*r1.p, self.q*r1.q)
    
    #Es para saber si es una fracción propia, y el siguiente para saber si es impropia    
    def es_propia(self):
        #Nos va a regresar un valor de tipo booleano
        return self.p < self.q
    def es_impropia(self):
        #Nos va a regresar un valor de tipo booleano
        return self.p > self.q
    
#Algo muy importante de las clases son los setters y los getters, que se explican a continuación:
        
    #Datos Públicos vs Datos Privados
    #Un atributo es privado cuando solo se puede acceder a el dentro de la clase que esta definida
    #es publico cuando se puede accceder a él fuera de la clase
    
    #Getter y Setter: El getter se usa para obtener el valor de los datos
    #el setter se usa para cambiar el valor de los datos
    def setP(self, numerador):
        if isinstance(numerador, int):
            self.p = numerador
        else:
            print('El numerador debe ser entero')
        self.p = numerador
        
    def setQ(self, denominador):
        if isinstance(denominador, int) and denominador  != 0 and denominador >0 :
            self.q = denominador
        else:
            print('El denominador debe ser entero y distinto de cero')
    
    def getP(self):
        return self.p
    def getQ(self):
        return self.q
    
    def mcd(a,b):
        while a % b != 0:
            resto = a%b
            a=b
            b=resto
        return b
    
    def simplificar(self):
        m = mcd(self.getP(), self.getQ())
        self.setP(self.getP()/m)
        self.setQ(self.getQ()/m)


    
"""Los metodos dunder son metodos que se pueden implementar en cualquier clase"""

""" Si se ejecuta como script la variable
 __name__ tiene la cadena "__main__", 
 si está ejecutado como módulo regresa el nombre 
 del archivo sin el .py
 """
#A la hora de importar esto a otro archivo, si no queremos quese hagan todos estos cálculos hacemos lo siguiente
if __name__ == "__main__":
#ya podemos crear un objeto de la clase racional con init y con str
    a = racional(1, 2)
    print(a)
    a.setP(-25)
    print('El valor de p es:', str(a.getP()))
    #Tenemos que definir mejor la clase para que esto no lo acepte
    b = racional('uno', 'dos')
    print(b)
    c = racional('uno', 0)
    print(c)
    d = racional(3,4)
    e = a*d
    print(e)

#Todas las clases deben tener un constructor, un dunder __str__
#que es la representación como cadena del objeto
