#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May 30 13:22:26 2020

@author: Lola Serrano
"""
import math
#%%
#% conda install -c conda-forge prettytable
class ciudad():
    def __init__(self, ciudad, mkw, plantas=[]):
        self.set_nombre(ciudad)
        self.set_demanda(mkw)
        self.set_provedores(plantas)
        
    def set_provedores(self, plantas):
        self.__provedores = {}
        for provedor,cantidad in plantas:
            self.agregar_proveedor(provedor, cantidad)
            
    def agregar_proveedor(self,planta, cantidad):
        self.__provedores[planta]=cantidad

    def set_nombre(self, nombre_ciudad):
        self.__nombre = nombre_ciudad
        
    def get_demanda(self):
        return self.__demanda
    
    def set_demanda(self, mkw):
        self.__demanda = mkw
    
    def get_nombre(self):
        return self.__nombre    
    def get_proveedores():
         pass      
    def __str__(self):
        l = ""
        if len(self.get_provedores()) > 0:
            l = "," + str(self.get_provedores())
        return self.get_nombre() + "," + str(self.get_demanda()) + l
    

class planta():
    def __init__(self,nombre_planta, mkw, clientes=[]):
        self.set_nombre(nombre_planta)
        self.set_oferta(mkw)
        # self.clientes = {}
        # for cliente,costo in clientes:
        #     #self.clientes[cliente]=costo
        #     self.set_cliente(cliente,costo,0)
        self.set_clientes(clientes)
    def set_nombre(self, nombre_planta):
        self.__nombre = nombre_planta
    def set_clienteCantidad(self,cliente, cantidad):
        self.set_cliente( cliente, self.get_clienteCosto(cliente),cantidad)

    def set_clientes(self, clientes):
        self.__clientes = {}
        for cliente,costo in clientes:
            self.set_cliente(cliente,costo,0)

    def set_cliente(self, cliente, costo, cantidad=0):
        self.__clientes[cliente] = [costo, cantidad]
    
    def set_oferta(self, mkw):
        self.__oferta = mkw
    
    def get_oferta(self):
        return self.__oferta

    def get_clientes(self):
        return self.__clientes
    def get_clienteCosto(self,cliente):
        return self.__clientes[cliente][0]
    
    def get_nombre(self):
        return self.__nombre
    
    def __str__(self):
        l = ""
        if len(self.get_clientes()) > 0:
            l = "," + str(self.get_clientes())
        return self.get_nombre() + "," + str(self.get_oferta()) + l     
    
class problema_transporte():
    def __init__(self,plantas, ciudades):
        self.set_origenes(plantas)
        self.set_destinos(ciudades)
        self.set_costoTotal(-1)
    def set_origenes(self, plantas):
        self.__origenes = plantas
    def set_destinos(self,ciudades):
        self.__destinos = ciudades
    def set_costoTotal(self, total):
        self.__costoTotal = total
    def calcula_costoTotal(self):
        ct = 0 
        for planta in self.get_origenes():
            for cliente, [costo, asignacion] in planta.get_clientes().items():
                ct += costo * asignacion
        self.set_costoTotal(ct)
    def get_origenes(self):
        return self.__origenes
    def get_destinos(self):
        return self.__destinos
    def get_costoTotal(self):
        """
        

        Returns
        -------
        TYPE
            DESCRIPTION.

        """
        return self.__costoTotal
                
    def esquina_noroeste(self):
        """
        Resuelve el problema del transporte usando el método 
        de la esquina noroeste

        Returns
        -------
        None.

        """
        for ciudad in self.get_destinos():
            
            #print(ciudad)
            #print(ciudad.get_demanda())
            for planta in self.get_origenes():
        
                if planta.get_oferta() >0 and planta.get_oferta() >= ciudad.get_demanda():
                    planta.set_clienteCantidad(ciudad.get_nombre(),ciudad.get_demanda())
                    planta.set_oferta(planta.get_oferta() - ciudad.get_demanda())
                    ciudad.set_demanda(0)
                    # print(planta.get_oferta() - ciudad.get_demanda())
                    # print(planta)
                    # input("Enter")
                    break
                elif ciudad.get_demanda() > 0 and  planta.get_oferta() > 0 :
                    planta.set_clienteCantidad( ciudad.get_nombre(), planta.get_oferta() )
                    ciudad.set_demanda( ciudad.get_demanda() - planta.get_oferta() )
                    planta.set_oferta(0) 
        self.calcula_costoTotal()
        
    def costo_minimo(self):
        CostoMinimoActual = math.inf
        plantaCostoMinmoActual = None
        ciudadDelCostoMinimo = None
        Plantas = self.get_origenes()
        for planta in Plantas:
            if planta.get_oferta()>0:
                clientes = planta.get_clientes()
                print (clientes)
                for cliente in planta.get_clientes():
                    costoAsig = clientes[cliente]
                    costo = costoAsig[0]
                    print (planta.get_nombre())
                    print ("\t"+cliente)
                    print ("\t Costo,Asignacion:"+ str(costoAsig))        
                    if costo <= CostoMinimoActual:
                        CostoMinimoActual = costo
                        plantaCostoMinmoActual = planta
                        ciudadDelCostoMinimo = cliente
             
        self.calcula_costoTotal() 
        
    def costo_minimo_por_ciudad(self):
        """

        Returns
        -------
        None.

        """
        for ciudad in self.get_destinos():
            Plantas = self.get_origenes()
            origenes = sorted(Plantas,key=lambda Plantas: Plantas.get_clientes()[ciudad.get_nombre()])            
            #print(ciudad)
            #print(ciudad.get_demanda())
            #for planta in self.get_origenes():
            for planta in origenes:
        
                if planta.get_oferta() >0 and planta.get_oferta() >= ciudad.get_demanda():
                    planta.set_clienteCantidad(ciudad.get_nombre(),ciudad.get_demanda())
                    planta.set_oferta(planta.get_oferta() - ciudad.get_demanda())
                    ciudad.set_demanda(0)
                    # print(planta.get_oferta() - ciudad.get_demanda())
                    # print(planta)
                    # input("Enter")
                    break
                elif ciudad.get_demanda() > 0 and  planta.get_oferta() > 0 :
                    planta.set_clienteCantidad( ciudad.get_nombre(), planta.get_oferta() )
                    ciudad.set_demanda( ciudad.get_demanda() - planta.get_oferta() )
                    planta.set_oferta(0) 
        self.calcula_costoTotal()        
        
    def mostrar(self):
        from prettytable import PrettyTable

        
        x = PrettyTable()
        x.field_names = ["Planta"] + [ciudad.get_nombre() for ciudad in self.get_destinos()] + ["OFERTA"]
        
        i = 0
        for planta in self.get_origenes():    
            costos = [planta.get_clientes()[ciudad] for ciudad in planta.get_clientes()] + [planta.get_oferta()]
            x.add_row([planta.get_nombre()] + costos )
            i += 1
        x.add_row(["Demanda "] + [ciudad.get_demanda() for ciudad in self.get_destinos()] + [""])
        print(x)
        print("Costo total: " + str(self.get_costoTotal()))
        
