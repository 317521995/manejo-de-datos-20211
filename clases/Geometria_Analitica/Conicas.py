# -*- coding: utf-8 -*-
"""
Created on Wed Jan 13 01:19:47 2021

@author: 
Kevin Hernández Astudillo
Iván Organista Dionicio
Lola Serrano Carrasco
"""

import math
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from tabulate import tabulate

class EcuacionGeneral():
    """
    EcuacionGeneral es una clase que representa la ecuacion general de segundo
    grado en dos variables
    """
    
    '''El método constructor recibe los diferentes coeficientes de la cónica'''
    def __init__(self,A=0,B=0,C=0,D=0,E=0,F=0):
        self.set_cuadraticoX(A)
        self.set_terminocruzado(B)
        self.set_cuadraticoY(C)
        self.set_linealX(D)
        self.set_linealY(E)
        self.set_Constante(F)
    
    '''En los siguientes setters se fijan los diferentes términos'''
    def set_cuadraticoX(self,A):
        if isinstance(A,float) or isinstance(A,int):    
            self.__cuadraticoX=A
        else:
            print("Ingresar un valor numerico")

    def set_terminocruzado(self, B):
        if isinstance(B,float) or isinstance(B,int):
            self.__terminocruzado=B
        else:
            print("Ingresar un valor  numerico")
    def set_cuadraticoY(self,C):
        if isinstance(C,float) or isinstance(C,int):
            self.__cuadraticoY= C
        else:
            print("Ingresar un valor numerico")
        self.__cuadraticoY= C
    def set_linealX(self, D):
        if isinstance(D,float) or isinstance(D,int):
            self.__linealX=D
        else:
            print("Ingresar un valor numerico")
    
    def set_linealY(self, E):
        self.__linealY=E
    def set_Constante(self, F):
        if isinstance(F,float) or isinstance(F,int):
            self.__Constante= F
        else:
            print("Ingresar un valor numerico")
    
    '''Los getters recuperan los diferentes términos o coeficientes dados'''
    def get_cuadraticoX(self):
        return self.__cuadraticoX
    def get_terminocruzado(self):
        return self.__terminocruzado
    def get_cuadraticoY(self):
        return self.__cuadraticoY
    def get_linealX(self):
        return self.__linealX
    def get_linealY(self):
        return self.__linealY
    def get_Constante(self):
        return self.__Constante
    
    '''Nos regresa en una cadena con la ecuación de la canónica'''
    def __str__(self):
        #return self.get_cuadraticoX()"x^2"+self.get_terminocruzado()"xy"+self.get_cuadraticoY()"y^2"+self.get_linealX()"x"+self.get_linealY()"y"+self.get_Constante()
        return ""+str(self.get_cuadraticoX()) +"x^2 + "+ str(self.get_terminocruzado())+"xy + " +str(self.get_cuadraticoY())+"y^2 + "+str(self.get_linealX())+"x + "+str(self.get_linealY())+"y + "+str(self.get_Constante())
    
    '''Para definir la suma entre dos cónicas'''
    def __add__(self,e):
        """
        Es el metodo que utilizaremos para sumar dos objetos de la clase EcuacionGeneral

        Parameters
        ----------
        e : Objeto de la clase EcuacionGeneral
            DESCRIPTION.

        Returns
        -------
        TYPE
            DESCRIPTION.

        """
        a=float(self.get_cuadraticoX())
        b=float(self.get_terminocruzado())
        c=float(self.get_cuadraticoY())
        d=float(self.get_linealX())
        e1=float(self.get_linealY())
        f=float(self.get_Constante())
        return EcuacionGeneral(a + e.get_cuadraticoX(),b+ e.get_terminocruzado(), c + e.get_cuadraticoY(),d+ e.get_linealX(),e1+ e.get_linealY(),f+ e.get_Constante())
    
    '''Para saber de qué cónica se trata'''
    def QueConicaEs(self):
        """
        Es un metodo de la clase EcuacionGeneral que nos dice que 
        conica representan los parametros que acepta un objeto de dicha clase

        Parameters
        ----------
        e : Objeto de la clase EcuacionGeneral
            

        Returns
        -------
        Regrasa una lista que nos dice de que conica se trata
            DESCRIPTION.
            
        El metodo se basa en lo siguiente hecho y es que podemos saber de que conica se trata mediante el discriminante de la Ecuacion B^2 - 4AC
        Si pasa que  B^2 - 4AC< 0 se trata de una elipse
        si pasa que B^2 - 4AC>0 se trata de una hiperbola
        si pasa que B^2 - 4AC=0 se trata de una parabola
        http://recursostic.educacion.es/descartes/web/materiales_didacticos/Lugares_geometricos_conicas/ecu_gen.htm#:~:text=Las%20gr%C3%A1ficas%20de%20todas%20las,determina%20el%20tipo%20de%20curva.
        
    

        """
        d= (self.get_terminocruzado()**2)-(4*self.get_cuadraticoX()*self.get_cuadraticoY() )
        if  d<0:
            if float(self.get_cuadraticoX())!= float(self.get_cuadraticoY()):
                return "La Ecuacion Representa una Elipse"
            elif float( self.get_cuadraticoX())== float(self.get_cuadraticoY()) and float(self.get_terminocruzado())==0:
                return "La ecuacion representa una Circunferencia"
            else:
                return "Algun caso degenerado de la elipse"
        elif d>0:
            return "La Ecuacion representa una Hiperbola"
        elif d==0 and float(self.get_terminocruzado())==0 and (float(self.get_cuadraticoX())!=0 or float(self.get_cuadraticoY())!=0 ):
            return "La ecuacion representa una Parabola"
        elif 0==float(self.get_terminocruzado()) ==float(self.get_cuadraticoX())==float(self.get_cuadraticoY()):
            return "No se trata de una Conica (tiene A=B=C=0)"
        else:
            return "Este caso no está bien definido, trata de simplificar y eliminar términos cruzados." 

    def Circunferencia(self):
        """
        Este metodo de la clase Ecuacion General nos regresa una lista con algunos ragos importantes de la circunferencia

        Parameters
        ----------
        c : TYPE 
        Obejto de la clase EcuacionGeneral


        Returns
        -------
        TYPE 
        str con el centro de la circunferencia, su radio y su ecuacion en forma canonica
        

        """
        a=float(self.get_cuadraticoX())
        b=float(self.get_terminocruzado())
        c=float(self.get_cuadraticoY())
        d=float(self.get_linealX())
        e=float(self.get_linealY())
        f=float(self.get_Constante())

        termino_de_x=(d)/(2*a)
        termino_de_y=(e)/(2*c)
        radiocuadrado=((c*d**2)+(a*e**2)-(4*a*c*f))/(4*(a**2)*c)
        radio=math.sqrt(radiocuadrado)
        k=(self.get_linealX()**2)/4  
        l=(self.get_linealY()**2)/4
        if b==0 and a==1 and c==1 and ((k+l)-f) >0  and (b**2 -4*a*c)<0:
            termino_de_x=(d)/(2*a)
            termino_de_y=(e)/(2*c)
            c1=(termino_de_x *-1,termino_de_y *-1)
            radiocuadrado=((c*d**2)+(a*e**2)-(4*a*c*f))/(4*(a**2)*c)
            radio=math.sqrt(radiocuadrado)
            return [(c1),radio," (x + "+str(termino_de_x)+")^2 + (y + "+str(termino_de_y)+")^2 = " +str(radiocuadrado)]
    
        elif b==0 and a!=1 and c!=1 and (k+l)-f >0 and (b**2 -4*a*c)<0 and a==c:
            termino_de_x=(d)/(2*a)
            termino_de_y=(e)/(2*c)
            c1=(termino_de_x *-1,termino_de_y *-1)
            radiocuadrado=((c*d**2)+(a*e**2)-(4*a*c*f))/(4*(a**2)*c)
            radio=math.sqrt(radiocuadrado)
            return [(c1),radio," (x + "+str(termino_de_x)+")^2 + (y + "+str(termino_de_y)+")^2 = " +str(radiocuadrado)]
    
        else: 
            return "No es circunferencia "

    def Parabola(self):
        '''
        Returns: Lista con información importante sobre la parábola ingresada
        
        Tipo: Lista
         
        El método Parabola() de la clase Ecuación General regresa en una lista
        si es una parábola horizontal o vertical, la coordenada del foco, la coordenada
        del vértice y su función en forma canónica

        '''
        a=round(float(self.get_cuadraticoX()),3)
        b=round(float(self.get_terminocruzado()),3)
        c=round(float(self.get_cuadraticoY()),3)
        d=round(float(self.get_linealX()),3)
        e=round(float(self.get_linealY()),3)
        f=round(float(self.get_Constante()),3)
        
        #Parabola vertical
        if a != 0 and c != 0:
            return 'No es una parábola'
        if b != 0:
            return 'No es una parábola  '
        if a != 0 and c == 0:
            if e == 0:
                return 'No es una parábola'
            else:
                h = -round(d/(2*a),3)
                k = round(( ((d)/(2*a))**2 - f/a)/(e/a),3)
                vertice = (h,k)
                p = round(-e/(a*4),3)
                p4 = 4*p
                foco = (h, k + p)
                canonica = '(x-'+ str(h)+')^2 = '+ str(p4) + '(y+'+str(k)+')'
                return ['Parábola vertical', foco, vertice, canonica ]
        if c != 0 and a == 0:
            if b != 0:
                return 'No es una parábola' 
            if c != 0 and d == 0:
                return 'No es una parabola'
            else:
                k = -e/(2*c)
                h = ((e/(2*c))**2 - (f/c))/(d/c)
                vertice = (h,k)
                p = -d/(4*c)
                p4 = p*4
                foco = (h + p, k)
                canonica = '(y-'+ str(k)+')^2 = '+ str(p4) + '(x -'+str(h)+')'
                return ['Parábola horizontal', foco, vertice, canonica ]
     
    def Elipse(self):
        """
        Este método es una herramienta para saber los elementos de una Elipse, nos regresá una lista de la siguiente forma:
            ["Elipse",centro,focos,vertices]
            
        Donde, como su nombre lo indica, nos da cuatro elementos, el priemro es una cadena que nos dice qué se trata de la Elipse, 
        la seugnda variable, llamada centro, nos una tupla que interpretamos como una pareja oredenada en R2.
        
        Para la tercera variable de la lista, llamada focos, tenemos que esta nos regresa una tupla de dos objetos donde cada una representa 
        un foco de la respectiva elipse.
        
        Por último, tenemos que la variable nos regresa, como su nombre lo indica, los vértices, representados por dos tuplas, 
        cada una representando una pareja ordenada en R2.
        
        Para saber más de cómo se consideraron los casos pudes visitar:
            http://www.prepa5.unam.mx/wwwP5/profesor/publicacionMate/11VII.pdf

        Parameters
        ----------
        c : obejto de la clase EcuacionGeneral
            DESCRIPTION.
            En un principio todas las elipses tiene que tener el primer y tercer parametro distintos de cero
        Returns
        -------
        TYPE:list
            DESCRIPTION.

        """
        
        a=round(float(self.get_cuadraticoX()),3)
        b=round(float(self.get_terminocruzado()),3)
        c=round(float(self.get_cuadraticoY()),3)
        d=round(float(self.get_linealX()),3)
        e=round(float(self.get_linealY()),3)
        f=round(float(self.get_Constante()),3)

        if b==0 and (a!=0) and (c!=0) and (d!=0) and (e!=0) and (f!=0):
            v1=round((d)/(2*a),3)
            v2=round((((d**2)*(c))+((e**2)*a)-(4*a*c*f))/(4*(a**2)*c),3)
            u1=round((e/(2*c)),3)
            u2=round((((d**2)*(c))+((e**2)*a)-(4*a*c*f))/(4*(a)*(c**2)),3)
            if (v2 >= u2 or u2>v2) and ( v2<0 or u2<0) :
                return "no se trata de una elipse"
            elif a>c:
                if v2 >= u2 and v2>0 and u2>0:
                    h=round(-1*v1,3)
                    k=round(-1*u1,3)
                    a=round(math.sqrt(v2),3)
                    b=round(math.sqrt(u2),3)
                    c=round(math.sqrt(v2-u2),3)
                    centro=(h,k)
                    focos=(h,round(k+c,3)),(h,round(k-c,3))
                    vertices=(h,round(k+a,3)),(h,round(k-a,3))
                    return ["Elipse",centro,focos,vertices," (x+ "+str(v1)+")^2/("+str(v2)+") +  (y+"+str(u1)+")^2 / ("+str(u2)+") =  1"]
                elif u2>v2 and u2>0 and v2>0:
                    h=round(-1*v1,3)
                    k=round(-1*u1,3)
                    a=round(math.sqrt(v2),3)
                    b=round(math.sqrt(u2),3)
                    c=round(math.sqrt(u2-v2),3)
                    centro=(h,k)
                    focos=(h,round(k+c,3)),(h,round(k-c,3))
                    vertices=(h,round(k+a,3)),(h,round(k-a,3))
                    #[que conica es, centro, focosm vertices, longuitud del ejer mayor, longuitud del eje menor, excentricidad]
                    return ["Elipse",centro,focos,vertices," (x+ "+str(v1)+")^2/("+str(v2)+") +  (y+"+str(u1)+")^2 / ("+str(u2)+") =  1"]
                
                
            elif c>a:
                if v2 >= u2 and v2>0 and u2>0:
                    h=round(-1*v1,3)
                    k=round(-1*u1,3)
                    a=round(math.sqrt(v2),3)
                    b=round(math.sqrt(u2),3)
                    c=round(math.sqrt(v2-u2),3)
                    centro=(h,k)
                    focos=(round(h-c,3),k),(round(h+c,3),k)
                    vertices=(round(h-a,3),k),(round(h+a,3),k)
                    return ["Elipse",centro,focos,vertices," (x+ "+str(v1)+")^2/("+str(v2)+") +  (y+"+str(u1)+")^2 / ("+str(u2)+") =  1"]
                elif u2>v2 and u2>0 and v2>0:
                    h=round(-1*v1,3)
                    k=round(-1*u1,3)
                    a=round(math.sqrt(v2),3)
                    b=round(math.sqrt(u2),3)
                    c=round(math.sqrt(u2-v2),3)
                    centro=(h,k)
                    focos=(round(h+c,3),k),(round(h-c,3),k)
                    vertices=(round(h+a,3),k),(round(h-a,3),k)
                    #[que conica es, centro, focosm vertices, longuitud del ejer mayor, longuitud del eje menor, excentricidad]
                    return ["Elipse",centro,focos,vertices," (x+ "+str(v1)+")^2/("+str(v2)+") +  (y+"+str(u1)+")^2 / ("+str(u2)+") =  1"]
                
            
        elif b==0 and a!=0 and c!=0 and f!=0 and d==0  and e==0:
            v1=0
            v2=round((-1*f)/a,3)
            u1=0
            u2=round((-1*f)/c,3)
            if (v2 >= u2 or u2>v2) and ( v2<0 or u2<0) :
                return "no se trata de una elipse"
            elif a>c:
                if v2 >= u2 and v2>0 and u2>0:
                    h=round(-1*v1,3)
                    k=round(-1*u1,3)
                    a=round(math.sqrt(v2),3)
                    b=round(math.sqrt(u2),3)
                    c=round(math.sqrt(v2-u2),3)
                    centro=(h,k)
                    focos=(h,round(k+c,3)),(h,round(k-c,3))
                    vertices=(h,round(k+a,3)),(h,round(k-a,3))
                    return ["Elipse",centro,focos,vertices,"(x+ "+str(v1)+")^2/("+str(v2)+") +  (y+"+str(u1)+")^2 / ("+str(u2)+") =  1"]
                elif u2>v2 and u2>0 and v2>0:
                    h=round(-1*v1,3)
                    k=round(-1*u1,3)
                    a=round(math.sqrt(v2),3)
                    b=round(math.sqrt(u2),3)
                    c=round(math.sqrt(u2-v2),3)
                    centro=(h,k)
                    focos=(h,round(k+c,3)),(h,round(k-c,3))
                    vertices=(h,round(k+a,3)),(h,round(k-a,3))
                    #[que conica es, centro, focosm vertices, longuitud del ejer mayor, longuitud del eje menor, excentricidad]
                    return ["Elipse",centro,focos,vertices," (x+ "+str(v1)+")^2/("+str(v2)+") +  (y+"+str(u1)+")^2 / ("+str(u2)+") =  1"]
                
                
            elif c>a:
                if v2 >= u2 and v2>0 and u2>0:
                    h=round(-1*v1,3)
                    k=round(-1*u1,3)
                    a=round(math.sqrt(v2),3)
                    b=round(math.sqrt(u2),3)
                    c=round(math.sqrt(v2-u2),3)
                    centro=(h,k)
                    focos=(round(h-c,3),k),(round(h+c,3),k)
                    vertices=(round(h-a,3),k),(round(h+a,3),k)
                    return ["Elipse",centro,focos,vertices," (x+ "+str(v1)+")^2/("+str(v2)+") +  (y+"+str(u1)+")^2 / ("+str(u2)+") =  1"]
                elif u2>v2 and u2>0 and v2>0:
                    h=round(-1*v1,3)
                    k=round(-1*u1,3)
                    a=round(math.sqrt(v2),3)
                    b=round(math.sqrt(u2),3)
                    c=round(math.sqrt(u2-v2),3)
                    centro=(h,k)
                    focos=(round(h+c,3),k),(round(h-c,3),k)
                    vertices=(round(h+a,3),k),(round(h-a,3),k)
                    #[que conica es, centro, focosm vertices, longuitud del ejer mayor, longuitud del eje menor, excentricidad]
                    return ["Elipse",centro,focos,vertices," (x+ "+str(v1)+")^2/("+str(v2)+") +  (y+"+str(u1)+")^2 / ("+str(u2)+") =  1"]
                
        elif b==0 and a!=0 and c!=0 and f==0 and d!=0 and e!=0:
            v1=round((d)/(2*a),3)
            v2=round((((d**2)*(c))+((e**2)*a))/(4*(a**2)*c),3)
            u1=round((e/(2*c)),3)
            u2=round((((d**2)*(c))+((e**2)*a))/(4*a*(c**2)),3)
            if c>a:
                if v2 >= u2:
                    h=round(-1*v1,3)
                    k=round(-1*u1,3)
                    a=round(math.sqrt(v2),3)
                    b=round(math.sqrt(u2),3)
                    c=round(math.sqrt(v2-u2),3)
                    centro=(h,k)
                    focos=(round(h-c,3),k),(round(h+c,3),k)
                    vertices=(round(h-a,3),k),(round(h+a,3),k)
                    return ["Elipse",centro,focos,vertices," (x+ "+str(v1)+")^2/("+str(v2)+") +  (y+"+str(u1)+")^2 / ("+str(u2)+") =  1"]
                elif u2>v2:
                    h=round(-1*v1,3)
                    k=round(-1*u1,3)
                    a=round(math.sqrt(v2),3)
                    b=round(math.sqrt(u2),3)
                    c=round(math.sqrt(u2-v2),3)
                    centro=(h,k)
                    focos=(round(h+c,3),k),(round(h-c,3),k)
                    vertices=(round(h+a,3),k),(round(h-a,3),k)
                    #[que conica es, centro, focosm vertices, longuitud del ejer mayor, longuitud del eje menor, excentricidad]
                    return ["Elipse",centro,focos,vertices,"(x+ "+str(v1)+")^2/("+str(v2)+") +  (y+"+str(u1)+")^2 / ("+str(u2)+") =  1"]
            if a>c:
                if v2 >= u2:
                    h=round(-1*v1,3)
                    k=round(-1*u1,3)
                    a=round(math.sqrt(v2),3)
                    b=round(math.sqrt(u2),3)
                    c=round(math.sqrt(v2-u2),3)
                    centro=(h,k)
                    focos=(h,round(k+c,3)),(round(h+c,3),round(k-c,3))
                    vertices=(h,round(k+a,3)),(h,round(k-a,3))
                    return ["Elipse",centro,focos,vertices," (x+ "+str(v1)+")^2/("+str(v2)+") +  (y+"+str(u1)+")^2 / ("+str(u2)+") =  1"]
                elif u2>v2:
                    h=round(-1*v1,3)
                    k=round(-1*u1,3)
                    a=round(math.sqrt(v2),3)
                    b=round(math.sqrt(u2),3)
                    c=round(math.sqrt(u2-v2),3)
                    centro=(h,k)
                    focos=(h,round(k-c,3)),(h,round(k+c,3))
                    vertices=(h,round(k-a,3)),(h,round(k+a,3))
                    #[que conica es, centro, focosm vertices, longuitud del ejer mayor, longuitud del eje menor, excentricidad]
                    return ["Elipse",centro,focos,vertices," (x+ "+str(v1)+")^2/("+str(v2)+") +  (y+"+str(u1)+")^2 / ("+str(u2)+") =  1"]
                
                
        elif b==0 and a!=0 and c!=0 and f!=0 and d!=0  and e==0:
            v1=round((d/(2*a)),3)
            u1=0
            v2=round(((d**2)+(-1*4*a*f))/(4*(a**2)),3)
            u2=round(((d**2)+(-1*4*a*f))/(4*a*c),3)
            if a>c:
                if v2>= u2 and  u2>0 and v2>0 :
                    h=round(-1*v1,3)
                    k=round(-1*u1,3)
                    a=round(math.sqrt(v2),3)
                    b=round(math.sqrt(u2),3)
                    c=round(math.sqrt(v2-u2),3)
                    centro=(h,k)
                    focos=(h,k),(h,k)
                    vertices=(h,k),(h,k)
                    return ["Elipse",centro,focos,vertices," (x+ "+str(v1)+")^2/("+str(v2)+") +  (y+"+str(u1)+")^2 / ("+str(u2)+") =  1"]
                    
                elif u2>v2 and u2>0 and v2>0:
                    h=round(-1*v1,3)
                    k=round(-1*u1,3)
                    a=round(math.sqrt(v2),3)
                    b=round(math.sqrt(u2),3)
                    c=round(math.sqrt(u2-v2),3)
                    centro=(h,k)
                    focos=(h,round(k+c,3)),(h,round(k-c,3))
                    vertices=(h,round(k+a,3)),(h,round(k-a,3))
                    #[que conica es, centro, focosm vertices, longuitud del ejer mayor, longuitud del eje menor, excentricidad]
                    return ["Elipse",centro,focos,vertices," (x+ "+str(v1)+")^2/("+str(v2)+") +  (y+"+str(u1)+")^2 / ("+str(u2)+") =  1"]
        
            elif c>a:
                if v2>= u2 and  u2>0 and v2>0 :
                    h=round(-1*v1,3)
                    k=round(-1*u1,3)
                    a=round(math.sqrt(v2),3)
                    b=round(math.sqrt(u2),3)
                    c=round(math.sqrt(v2-u2),3)
                    centro=(h,k)
                    focos=(round(h-c,3),k),(round(h+c,3),k)
                    vertices=(round(h-a,3),k),(round(h+a,3),k)
                    return ["Elipse",centro,focos,vertices," (x+ "+str(v1)+")^2/("+str(v2)+") +  (y+"+str(u1)+")^2 / ("+str(u2)+") =  1"]
                    
                elif u2>v2 and u2>0 and v2>0:
                    h=round(-1*v1,3)
                    k=round(-1*u1,3)
                    a=round(math.sqrt(v2),3)
                    b=round(math.sqrt(u2),3)
                    c=round(math.sqrt(u2-v2),3)
                    centro=(h,k)
                    focos=(round(h-c,3),k),(round(h+c,3),k)
                    vertices=(round(h-a,3),k),(round(h+a,3),k)
                    #[que conica es, centro, focosm vertices, longuitud del ejer mayor, longuitud del eje menor, excentricidad]
                    return ["Elipse",centro,focos,vertices, "(x+ "+str(v1)+")^2/("+str(v2)+") +  (y+"+str(u1)+")^2 / ("+str(u2)+") =  1"]
        
        elif b==0 and a!=0 and c!=0 and f!=0 and d==0  and e!=0:
            v1=0
            u1=round((e)/(2*c),3)
            v2=round(((e**2)+(-1*4*c*f))/(4*c*a),3)
            u2=round(((e**2)+(-1*4*c*f))/(4*(c**2)),3)
            if c>a:
                if v2>= u2 and  u2>0 and v2>0 :
                    h=round(-1*v1,3)
                    k=round(-1*u1,3)
                    a=round(math.sqrt(v2),3)
                    b=round(math.sqrt(u2),3)
                    c=round(math.sqrt(v2-u2),3)
                    centro=(h,k)
                    focos=(round(h-c,3),k),(round(h+c,3),k)
                    vertices=(round(h-a,3),k),(round(h+a,3),k)
                    return ["Elipse",centro,focos,vertices," (x+ "+str(v1)+")^2/("+str(v2)+") +  (y+"+str(u1)+")^2 / ("+str(u2)+") =  1"]
                    
                elif u2>v2 and u2>0 and v2>0:
                    h=round(-1*v1,3)
                    k=round(-1*u1,3)
                    a=round(math.sqrt(v2),3)
                    b=round(math.sqrt(u2),3)
                    c=round(math.sqrt(u2-v2),3)
                    centro=(h,k)
                    focos=(round(h+c,3),k),(round(h-c,3),k)
                    vertices=(round(h+a,3),k),(round(h-a,3),k)
                    #[que conica es, centro, focosm vertices, longuitud del ejer mayor, longuitud del eje menor, excentricidad]
                    return ["Elipse",centro,focos,vertices," (x+ "+str(v1)+")^2/("+str(v2)+") +  (y+"+str(u1)+")^2 / ("+str(u2)+") =  1"]
            elif a>c:
                if v2>= u2 and  u2>0 and v2>0 :
                    h=round(-1*v1,3)
                    k=round(-1*u1,3)
                    a=round(math.sqrt(v2),3)
                    b=round(math.sqrt(u2),3)
                    c=round(math.sqrt(v2-u2),3)
                    centro=(h,k)
                    focos=(h,round(k+c,3)),(h,round(k-c,3))
                    vertices=(h,round(k+a,3)),(h,round(k-a,3))
                    return ["Elipse",centro,focos,vertices," (x+ "+str(v1)+")^2/("+str(v2)+") +  (y+"+str(u1)+")^2 / ("+str(u2)+") =  1"]
                    
                elif u2>v2 and u2>0 and v2>0:
                    h=round(-1*v1,3)
                    k=round(-1*u1,3)
                    a=round(math.sqrt(v2),3)
                    b=round(math.sqrt(u2),3)
                    c=round(math.sqrt(u2-v2),3)
                    centro=(h,k)
                    focos=(h,round(k+c,3)),(h,round(k-c,3))
                    vertices=(h,round(k+a,3)),(h,round(k-a,3))
                    #[que conica es, centro, focosm vertices, longuitud del ejer mayor, longuitud del eje menor, excentricidad]
                    return ["Elipse",centro,focos,vertices,"(x+ "+str(v1)+")^2/("+str(v2)+") +  (y+"+str(u1)+")^2 / ("+str(u2)+") =  1"]
           
        elif b==0 and a!=0 and c!=0 and f==0 and d!=0  and e==0:
            v1=round((d/(2*a)),3)
            v2=round((d**2)/(4*(a**2)),3)
            u1=0
            u2=round((d**2)/(4*a*c),3)
            if a>b:
                if v2>=u2:
                    h=round(-1*v1,3)
                    k=round(-1*u1,3)
                    a=round(math.sqrt(v2),3)
                    b=round(math.sqrt(u2),3)
                    c=round(math.sqrt(v2-u2),3)
                    centro=(h,k)
                    focos=(h,k+c),(h,k-c)
                    focos=(h,round(k+c,3)),(h,round(k-c,3))
                    vertices=(h,round(k+a,3)),(h,round(k-a,3))
                    return ["Elipse",centro,focos,vertices," (x+ "+str(v1)+")^2/("+str(v2)+") +  (y+"+str(u1)+")^2 / ("+str(u2)+") =  1"]
                if u2>v2:
                    h=round(-1*v1,3)
                    k=round(-1*u1,3)
                    a=round(math.sqrt(u2),3)
                    b=round(math.sqrt(v2),3)
                    c=round(math.sqrt(u2-v2),3)
                    centro=(h,k)
                    focos=(h,round(k+c,3)),(h,round(k-c,3))
                    vertices=(h,round(k+a,3)),(h,round(k-a,3))
                    return ["Elipse es esta mera",centro,focos,vertices," (x+ "+str(v1)+")^2/("+str(v2)+") +  (y+"+str(u1)+")^2 / ("+str(u2)+") =  1"]
            
                
            elif b>a:
                if v2>=u2:
                    h=round(-1*v1,3)
                    k=round(-1*u1,3)
                    a=round(math.sqrt(v2),3)
                    b=round(math.sqrt(u2),3)
                    c=round(math.sqrt(v2-u2),3)
                    centro=(h,k)
                    focos=(round(h+c,3),k),(round(h-c,3),k)
                    vertices=(round(h+a,3),k),(round(h-a,3),k)
                    return ["Elipse",centro,focos,vertices," (x+ "+str(v1)+")^2/("+str(v2)+") +  (y+"+str(u1)+")^2 / ("+str(u2)+") =  1"]
                if u2>v2:
                    h=round(-1*v1,3)
                    k=round(-1*u1,3)
                    a=round(math.sqrt(u2),3)
                    b=round(math.sqrt(v2),3)
                    c=round(math.sqrt(u2-v2),3)
                    centro=(h,k)
                    focos=(round(h+c,3),k),(round(h-c,3),k)
                    vertices=(round(h+a,3),k),(round(h-a,3),k)
                    return ["Elipse es esta mera",centro,focos,vertices," (x+ "+str(v1)+")^2/("+str(v2)+") +  (y+"+str(u1)+")^2 / ("+str(u2)+") =  1"]
            
        elif b==0 and a!=0 and c!=0 and f==0 and d==0  and e!=0:
            v1=0
            v2=round((e**2)/(4*a*c),3)
            u1=round((e/(2*c)),3)
            u2=round((e**2)/(4*(c**2)),3)
            if a>c:
                if v2>=u2 and u2>=0 and v2>=0:
                    h=round(-1*v1,3)
                    k=round(-1*u1,3)
                    a=round(math.sqrt(v2),3)
                    b=round(math.sqrt(u2),3)
                    c=round(math.sqrt(v2-u2),3)
                    centro=(h,k)
                    focos=(h,round(k+c,3)),(h,round(k-c,3))
                    vertices=(h,round(k+a,3)),(h,round(k-a,3))
                    return ["Elipse",centro,focos,vertices,"(x+ "+str(v1)+")^2/("+str(v2)+") +  (y+"+str(u1)+")^2 / ("+str(u2)+") =  1"]
                if u2>v2 and u2>=0 and v2>=0:
                    h=round(-1*v1,3)
                    k=round(-1*u1,3)
                    a=round(math.sqrt(u2),3)
                    b=round(math.sqrt(v2),3)
                    c=round(math.sqrt(u2-v2),3)
                    centro=(h,k)
                    focos=(h,round(k+c,3)),(h,round(k-c,3))
                    vertices=(h,round(k+a,3)),(h,round(k-a,3))
                    return ["Elipse",centro,focos,vertices," (x+ "+str(v1)+")^2/("+str(v2)+") +  (y+"+str(u1)+")^2 / ("+str(u2)+") =  1"]
                
            elif c>a:
                if v2>=u2 and u2>=0 and v2>=0:
                    h=round(-1*v1,3)
                    k=round(-1*u1,3)
                    a=round(math.sqrt(v2),3)
                    b=round(math.sqrt(u2),3)
                    c=round(math.sqrt(v2-u2),3)
                    centro=(h,k) 
                    focos=(round(h+c,3),k),(round(h-c,3),k)
                    vertices=(round(h+a,3),k),(round(h-a,3),k)
                    return ["Elipse",centro,focos,vertices,"(x+ "+str(v1)+")^2/("+str(v2)+") +  (y+"+str(u1)+")^2 / ("+str(u2)+") =  1"]
                if u2>v2 and u2>=0 and v2>=0:
                    h=round(-1*v1,3)
                    k=round(-1*u1,3)
                    a=round(math.sqrt(u2),3)
                    b=round(math.sqrt(v2),3)
                    c=round(math.sqrt(u2-v2),3)
                    centro=(h,k)
                    focos=(round(h+c,3),k),(round(h-c,3),k)
                    vertices=(round(h+a,3),k),(round(h-a,3),k)
                    return ["Elipse",centro,focos,vertices,"(x+ "+str(v1)+")^2/("+str(v2)+") +  (y+"+str(u1)+")^2 / ("+str(u2)+") =  1"]
                
               
        elif (b**2 -4*a*c)<0 and a==c:
            return "Es un caso en particular de la elipse llamado circunferencia"
        elif a!=0 and c!=0 and b==d==e==f==0:
            return "se trata de un solo punto llamado origen"
        else:
            return "No es un Elipse"
         
    def Hiperbola(self):
        """
        Este metodo es un herramienta para saber los elementos de una hipérbola, nos regresa una lista de la siguiente forma:
            ["Hipérbola",centro,focos,vertices, asíntotas]
        
        Donde, como su nombre lo indica, nos da 5 elementos, el primero es una cadena que nos dice que se trata de la Hipérbola, 
        la segunda variable, llamada centro, nos da una tupla que interpretamos como una pareja oredenada en R2.
        
        Para la tercera variable, de la lista llamada focos, tenemos que esta nos regresa dos tuplas, donde cada una representa 
        un foco de la respectiva hipérbola.
        
        Además, tenemos que la variable nos regresa los vertices, representados por dos tuplas, 
        cada una representando una pareja ordenada en R2.

        Por último, no contemplaremos los casos de hipérbolas rotadas, por lo que si se busca probar con alguna hipérbola
        que aún tenga término cruzado, primero rotarla para eliminar el término cruzado y después usar este método de hipérbolas.
        """
        a=round(float(self.get_cuadraticoX()),3)
        b=round(float(self.get_terminocruzado()),3)
        c=round(float(self.get_cuadraticoY()),3)
        d=round(float(self.get_linealX()),3)
        e=round(float(self.get_linealY()),3)
        f=round(float(self.get_Constante()),3)
        canonica = 0
        det= (self.get_terminocruzado()**2)-(4*self.get_cuadraticoX()*self.get_cuadraticoY() )
        if b!=0: #AL ELIMINAR EL TÉRMINO CRUZADO SE ACEPTA EL RIESGO DE QUE LA HIPÉRBOLA SE DEGENERE...
            return "Esta ecuación falta ser rotada"
            """Eq_rot = self.EliminarTerminoCruzado()
            a = round(float(Eq_rot.get_cuadraticoX()),3)
            b = round(float(Eq_rot.get_terminocruzado()),3)
            c = round(float(Eq_rot.get_cuadraticoY()),3)
            d = round(float(Eq_rot.get_linealX()),3)
            e = round(float(Eq_rot.get_linealY()),3)
            f = round(float(Eq_rot.get_constante()),3)"""
        elif det <0 or det ==0:
            return "No es una hipérbola"
        elif a ==0 or c==0:
            return "No es una hipérbola"
        elif a==0 and d==0:
            return "No es una hipérbola"
        elif c==0 and e==0:
            return "No es una hipérbola"
        else:
            h = -round(d/2*a,3)
            k = -round(e/2*c,3)
            a_Transversal = round(math.sqrt(((-f*4*a**2*c**2) + (d**2*c**2) + (e**2*a**2))/4*c*a**2),3)
            b_Conjugado = round(math.sqrt(((f*4*a**2*c**2) - (d**2*c**2) - (e**2*a**2))/4*a*c**2),3)
            c_Pitagorica = round(math.sqrt((a_Transversal**2)*(b_Conjugado**2)),3)
            vertices = (h+a_Transversal,k)
            focos = (h+c_Pitagorica, k)
            y_asintota1 = round((a_Transversal/b_Conjugado)+k,3)
            y_asintota2 = round(-(b_Conjugado/a_Transversal)+k,3)
            y_asintotas = (y_asintota1,y_asintota2)
            canonica = " (x-"+str(h) +")^2 / ("+ str(a_Transversal) +")  -  (y -" +str(k) +")^2 / (" + str(b_Conjugado) + ") =1"
            return ["Hipérbola horizontal", focos, vertices, y_asintotas, canonica]

    def Tabla(self):
        '''
        Returns: Regresa una tabla, usando la bibliteca tabulate, con los datos correspondientes
        a la cónica. 

        '''
        if self.QueConicaEs() == "La Ecuacion Representa una Elipse":
            tabla = [['Categoria', 'Centro', 'Focos', 'Vértices', 'Canónica'], 
                    [str(self.Elipse()[0]),str(self.Elipse()[1]),str(self.Elipse()[2]),str(self.Elipse()[3]),str(self.Elipse()[4])] ]
            #[self.Elipse()[0],self.Elipse()[1],self.Elipse()[2],self.Elipse()[3],self.Elipse()[4] ]
            return tabulate(tabla, headers = 'firstrow', tablefmt = 'fancy_grid')
        elif self.QueConicaEs() == "La ecuacion representa una Circunferencia":
            tabla = [['Categoria', 'Centro', 'Radio', 'Canónica'], 
                    ['Circunferencia',str(self.Circunferencia()[0]),str(self.Circunferencia()[1]),str(self.Circunferencia()[2]) ]]
            return tabulate(tabla, headers = 'firstrow', tablefmt = 'fancy_grid')

        elif self.QueConicaEs() == "La ecuacion representa una Parabola":
            tabla = [['Categoria', 'Foco', 'Vértice', 'Canónica'], 
                    [str(self.Parabola()[0]),str(self.Parabola()[1]),str(self.Parabola()[2]),str(self.Parabola()[3]) ]]
            return tabulate(tabla, headers = 'firstrow', tablefmt = 'fancy_grid')
                
        elif self.QueConicaEs() == "La Ecuacion representa una Hiperbola":
            tabla = [['Categoria', 'Focos', 'Vértices', 'Asíntotas', 'Canónicas'], 
                    [str(self.Hiperbola()[0]),str(self.Hiperbola()[1]),str(self.Hiperbola()[2]),str(self.Hiperbola()[3]),str(self.Hiperbola()[4]) ]]
            return tabulate(tabla, headers = 'firstrow', tablefmt = 'fancy_grid')

        
    def __mul__(self,k):
        '''
        Parameters: Toma como parámetros un objeto de la clase Ecuación General y un valor flotante
        
        k : Flotante
            
        Returns:
        Objeto de la clase Ecuación General que fue multiplicado por el valor flotante
        
        TYPE: Ecuación General
        
        DESCRIPTION: El método __muL__ sirve para multiplicar un objeto de la clase ecuación general
        por un parámetro k

        '''
        a=k*float(self.get_cuadraticoX())
        b=k*float(self.get_terminocruzado())
        c=k*float(self.get_cuadraticoY())
        d=k*float(self.get_linealX())
        e=k*float(self.get_linealY())
        f=k*float(self.get_Constante())
        return EcuacionGeneral(a , b, c , d , e , f)
    
    def __truediv__(self,k):
        '''
        Parameters: Toma como parámetros un objeto de la clase Ecuación General y un valor flotante
        
        k : Flotante, debe ser distinto de cero
            
        Returns:
        Objeto de la clase Ecuación General que fue dividido entre el valor flotante.
        Este debe de ser diferente de cero para que sí pueda regresar otro objeto Ecuacion General
        
        
        TYPE: Ecuación General
        
        DESCRIPTION: El método __truediv__ sirve para dividir un objeto de la clase ecuación general
       entre un flotante k, el cual debe de ser distinto de cero.


        '''
        if k == 0 :
            return'No es posible dividir entre 0'
        else:
            a=float(self.get_cuadraticoX())/k
            b=float(self.get_terminocruzado())/k
            c=float(self.get_cuadraticoY())/k
            d=float(self.get_linealX())/k
            e=float(self.get_linealY())/k
            f=float(self.get_Constante())/k
            return EcuacionGeneral(a , b, c , d , e , f)

    '''Para eliminar el término cruzado de la Ecuación General (En algunos casos la degenera)'''    
    def EliminarTerminoCruzado(self):
        a=float(self.get_cuadraticoX())
        b=float(self.get_terminocruzado())
        c=float(self.get_cuadraticoY())
        d=float(self.get_linealX())
        e=float(self.get_linealY())
        f=float(self.get_Constante())
        #theta = math.radians(45)
        #theta = math.degrees(theta)
        if a == c:
            theta = math.radians(45)
        else:
            Tangente = math.radians(b/(c-a))
            theta = math.radians(math.atan(Tangente)/2)
        a1 = a*math.cos(theta)**2+ (b*math.sin(theta)*math.cos(theta)) + c*math.sin(theta)**2
        b1 = (2*math.sin(theta)*math.cos(theta)*(a+c)) + (b*(math.cos(theta)**2 + math.sin(theta)**2))
        c1 = a*math.sin(theta)**2 - (b*math.cos(theta)*math.sin(theta)) + c*math.cos(theta)**2
        d1 = d*math.cos(theta) + e*math.sin(theta)
        e1 = -d*math.sin(theta) + e*math.cos(theta)
        #a1 = a-(b*math.sin(theta)*math.cos(theta))
        #c1 = c+(b*math.sin(theta)*math.cos(theta))
        #b1 = (b*math.cos(theta)**2)-(b*math.sin(theta)**2)
        #d1 = d*math.cos(theta) - e*math.sin(theta)
        #e1 = d*math.sin(theta) + e*math.cos(theta)
        f1 = f

        return a,b,c,d,e,f, "theta→→", theta, "La rotada→", a1, "con término cruzado →→→",b1,c1,d1,e1,f1

    def Graficarla(self):
        a=float(self.get_cuadraticoX())
        b=float(self.get_terminocruzado())
        c=float(self.get_cuadraticoY())
        d=float(self.get_linealX())
        e=float(self.get_linealY())
        f=float(self.get_Constante())
        
        x = np.linspace(-25, 25, 200) #min/max/Intervalos
        y = np.linspace(-25, 25, 200)
        x, y = np.meshgrid(x, y)

        if c !=a and (b!=0 or float(b) != 0.0):
            #theta = (math.atan(b/(c-a)))/2
            #theta = math.radians(45)
            #theta = math.degrees(theta)
            Tangente = math.radians(b/(c-a))
            theta = math.radians(math.atan(Tangente)/2)
            theta = math.radians(45)
            a1 = a-(b*math.sin(theta)*math.cos(theta))
            c1 = c+(b*math.sin(theta)*math.cos(theta))
            b1 = (b*math.cos(theta)**2)-(b*math.sin(theta)**2)
            d1 = d*math.cos(theta) - e*math.sin(theta)
            e1 = d*math.sin(theta) + e*math.cos(theta)
            f1 = f
            """a1 = a*math.cos(theta)**2+ (b*math.sin(theta)*math.cos(theta)) + c*math.sin(theta)**2
            b1 = (2*math.sin(theta)*math.cos(theta)*(a+c)) + (b*(math.cos(theta)**2 + math.sin(theta)**2))
            c1 = a*math.sin(theta)**2 - (b*math.cos(theta)*math.sin(theta)) + c*math.cos(theta)**2
            d1 = d*math.cos(theta) + e*math.sin(theta)
            e1 = -d*math.sin(theta) + e*math.cos(theta)
            f1 = f"""
        elif a == c and (b!=0 or float(b) != 0.0):
                theta = math.radians(45)
                a1 = a-(b*math.sin(theta)*math.cos(theta))
                c1 = c+(b*math.sin(theta)*math.cos(theta))
                b1 = (b*math.cos(theta)**2)-(b*math.sin(theta)**2)
                d1 = d*math.cos(theta) - e*math.sin(theta)
                e1 = d*math.sin(theta) + e*math.cos(theta)
                f1 = f
                """a1 = a*math.cos(theta)**2+ (b*math.sin(theta)*math.cos(theta)) + c*math.sin(theta)**2
                b1 = (2*math.sin(theta)*math.cos(theta)*(a+c)) + (b*(math.cos(theta)**2 + math.sin(theta)**2))
                c1 = a*math.sin(theta)**2 - (b*math.cos(theta)*math.sin(theta)) + c*math.cos(theta)**2
                d1 = d*math.cos(theta) + e*math.sin(theta)
                e1 = -d*math.sin(theta) + e*math.cos(theta)
                f1 = f"""

        elif b == 0 or int(b) == 0:
            a1 = a
            b1=b
            c1=c
            d1=d
            e1=e
            f1=f
        else:
            a1 = a
            b1=b
            c1=c
            d1=d
            e1=e
            f1=f

        d= (self.get_terminocruzado()**2)-(4*self.get_cuadraticoX()*self.get_cuadraticoY() )
        if  d<0: #Elipse o Circunferencia
            try:
                plt.axhline(0, alpha=.1) #Para crear los ejes de la gráfica
                plt.axvline(0, alpha=.1)
                if float(self.get_cuadraticoX())!= float(self.get_cuadraticoY()):
                    #Se confirma que es una elipse
                    plt.annotate("Azul~Rotada",(-5,17), color='blue')
                    plt.annotate(self,(-17,-15), color='k')
                    plt.contour(x, y,(a1*x**2 + b1*x*y + c1*y**2 + d1*x + e1*y + f1), levels=[80],linestyles='solid', colors='blue')
                    plt.contour(x, y,(a*x**2 + b*x*y + c*y**2 + d*x + e*y + f), levels=[80],linestyles='solid', colors='k')
                    plt.show()
                elif float( self.get_cuadraticoX())== float(self.get_cuadraticoY()) and float(self.get_terminocruzado())==0:
                    #Se confirma que es una circunferencia
                    plt.annotate("La circunferencia no tiene término cruzado",(-20,30), color='blue')
                    plt.annotate(self,(-17,-15), color='k')
                    x = np.linspace(-50, 50, 200) #min/max/Intervalos
                    y = np.linspace(-35, 35, 200)
                    x, y = np.meshgrid(x, y)
                    plt.contour(x, y,(a*x**2 + b*x*y + c*y**2 + d*x + e*y + f), levels=[80],linestyles='solid', colors='k')
                    plt.show()

                else:
                    return "No puedes seguir, esto solo es una línea y/o el origen."


            except ValueError:
                return "algo está raro Elipse"
        elif d>0: #Hipérbola
            x = np.linspace(-503, 503, 505) #min/max/Intervalos
            y = np.linspace(-503, 503, 505)
            x, y = np.meshgrid(x, y)
            try:
                plt.axhline(0, alpha=.1)
                plt.axvline(0, alpha=.1)
                plt.annotate("Azul~Rotada",(-5,17), color='blue')
                plt.annotate(self,(-450,-400), color='red')
                plt.contour(x, y,(a1*x**2 + b1*x*y + c1*y**2 + d1*x + e1*y + f1), levels=[80],linestyles='solid', colors='blue')
                plt.contour(x, y,(a*x**2 + b*x*y + c*y**2 + d*x + e*y + f), levels=[80],linestyles='solid', colors='k')
                plt.show()
            except ValueError:
                return "En algún valor se indeterminó tu función (Hipérbola)"
        elif d==0 and float(self.get_terminocruzado())==0:
            if float(self.get_cuadraticoX())!=0 or float(self.get_cuadraticoY())!=0 :
                try:
                    plt.axhline(0, alpha=.1)
                    plt.axvline(0, alpha=.1)
                    #plt.annotate("Azul~Rotada",(-5,17), color='blue')
                    plt.annotate(self,(-17,-18), color='k')
                    plt.contour(x, y,(a*x**2 + b*x*y + c*y**2 + d*x + e*y + f), levels=[80],linestyles='solid', colors='k')
                    #plt.contour(x, y,(a1*x**2 + b1*x*y + c1*y**2 + d1*x + e1*y + f1), levels=[80],linestyles='solid', colors='blue')
                    plt.show()
                except ValueError:
                    return "En algún valor se indeterminó tu función (Parábola)"
            elif float(self.get_cuadraticoX())!=0 and float(self.get_cuadraticoY())!=0 :
                try:
                    plt.axhline(0, alpha=.1)
                    plt.axvline(0, alpha=.1)
                    #plt.annotate("Azul~Rotada",(-5,17), color='blue')
                    plt.annotate(self,(-19,-22), color='k')
                    plt.contour(x, y,(a*x**2 + b*x*y + c*y**2 + d*x + e*y + f), levels=[80],linestyles='solid', colors='k')
                    #plt.contour(x, y,(a1*x**2 + b1*x*y + c1*y**2 + d1*x + e1*y + f1), levels=[80],linestyles='solid', colors='blue')
                    plt.show()
                except ValueError:
                    return "En algún valor se indeterminó tu función (Parábola)"
            else:
                return "Parece Parábola pero es más complicado que eso"

        elif 0==float(self.get_terminocruzado()) ==float(self.get_cuadraticoX())==float(self.get_cuadraticoY()):
            return "No se trata de una Conica (tiene A=B=C=0)"
        else:
            return "Este caso no está bien definido, trata de simplificar y eliminar términos cruzados." 

    
#%% #solo son prubas para ver que todo funciona
p1 = EcuacionGeneral(6,0,0,5,-4, 8)
print(p1.Parabola())
print(p1.Tabla())
print(p1*3)
print(p1/2)
#print(p1.Graficarla())
print("---Pruebas----rápidas----"*3)
x1 = EcuacionGeneral(1,3,1)
print(x1.Hiperbola())
print("----Para x2------"*3)
x2 = EcuacionGeneral(90,0,-10, -900, 160, 1529)
print(x2.Hiperbola())
print(x2.Tabla())
print(x2.EliminarTerminoCruzado())
#print(x2.Graficarla())
#circ=EcuacionGeneral(2,0,2,8,6,5)
#print(circ.Tabla())
print("---M-Á-S---P-R-U-E-B-A-S--"*4)
#c3=EcuacionGeneral(1,3,1) #Hipérbola**
#print(c3.Hiperbola())
#h5 = EcuacionGeneral(5,8,3,2,2,-15) #Hipérbola**
#print(h5.Hiperbola())

