# -*- coding: utf-8 -*-
"""
Created on Mon Oct  5 08:11:53 2020

@author: LAP-HP
"""
#Estamos recordando cómo funcionan las clases, un ejemplo puede ser donde los
#objetos son los números complejos

class complejos():
    #Voy a implementar una clase para los números complejos.
    #Estos se forman de una parte real y una imaginaria a+bi, donde
    #a es la parte real y bi la parte imaginaria
    
    #Primero el método constructor. Los atributos serán la parte real y la parte imaginaria
    def __init__(self, parte_real, parte_imaginaria):
        self.a = parte_real 
        self.b = parte_imaginaria
    
        
    #Ahora el método str que nos da una representación como cadena del objeto
    def __str__(self):
        return '{}+{}i'.format(self.a, self.b)

#Ahora lo probamos
c1 = complejos(3,1)
print(c1)