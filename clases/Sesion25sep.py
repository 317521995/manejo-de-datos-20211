# -*- coding: utf-8 -*-
"""
Created on Fri Sep 25 08:16:10 2020

@author: LAP-HP
"""
from Racionales import mcd
import Complejos
print(mcd(81,27))


print('La varieble __name__ en el archivo main.py contiene la cadena:'+__name__)

#Un archivo de python se puede usar de dos formas: una directa que es como script
#y una indirecta que es como módulo
#Si se está ejecutando como script el archivo con la variable __name__ va a ser main
#Si se está ejecutando como módulo el archivo con la variable __name__ va a ser el nombre del archivo

"""
Dunders
-Compraración: __lt__, __le__, __eq__, __ne__, __gt__, __ge__
-Representación: __str__, __repr__
-Lógico: __bool__
-Llamado como función: __call__
-Operaciones numericas: __add__, __sub__, __mul__, __matmul__
__truediv__, __floordiv__, __mod__, __divmod__, __pow__,
__lshift__, __rshift__, __and__, __xor__, __or__
"""